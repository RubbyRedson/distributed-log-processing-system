create table users ( 
id int IDENTITY(1,1) PRIMARY KEY, 
login varchar(100) NOT NULL, 
password varchar(100) NOT NULL 
); 
create table sources ( 
id int IDENTITY(1,1) PRIMARY KEY, 
user_id int REFERENCES users(id) NOT NULL, 
name varchar(100) NOT NULL, 
ip varchar(100) NOT NULL, 
login varchar(100), 
password varchar(100) 
); 
create table groups (
id int IDENTITY(1,1) PRIMARY KEY, 
user_id int REFERENCES users(id) NOT NULL, 
name varchar(100) 
); 
create table alerts (
id int IDENTITY(1,1) PRIMARY KEY, 
source_id int REFERENCES sources (id) NOT NULL, 
name varchar(100) NOT NULL, 
level varchar(25) NOT NULL, 
email varchar(100) NOT NULL, 
topic varchar(100) NOT NULL, 
template varchar(1023) NOT NULL, 
pattern varchar(255) 
); 
create table scripts ( 
id int IDENTITY(1,1) PRIMARY KEY, 
source_id int REFERENCES sources (id) NOT NULL, 
name varchar(100) NOT NULL, 
level varchar(25) NOT NULL, 
path varchar(255) NOT NULL, 
pattern varchar(255) 
); 
create table logs ( 
id int IDENTITY(1,1) PRIMARY KEY, 
source_id int REFERENCES sources (id) NOT NULL, 
level varchar(25) NOT NULL, 
text varchar(255), 
time datetime NOT NULL 
); 
create table source_groups ( 
source_id int REFERENCES sources (id) NOT NULL, 
group_id int REFERENCES groups (id) NOT NULL 
);