package ru.hse.gureevnd.ui.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.hse.gureevnd.ui.converters.EntityConverter;
import ru.hse.gureevnd.ui.entities.Level;
import ru.hse.gureevnd.ui.entities.User;
import ru.hse.gureevnd.ui.persistance.entity.Source;
import ru.hse.gureevnd.ui.persistance.service.GroupService;
import ru.hse.gureevnd.ui.persistance.service.MetricService;
import ru.hse.gureevnd.ui.persistance.service.SourceService;
import ru.hse.gureevnd.ui.persistance.service.UserService;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Nick on 5/16/2016.
 */
@Controller
public class MetricsController {

    @Autowired
    MetricService metricService;

    @Autowired
    UserService userService;

    @Autowired
    GroupService groupService;

    @Autowired
    SourceService sourceService;

    @Autowired
    EntityConverter entityConverter;

    private User currentUser;

    @RequestMapping(value = {"/metrics"}, method = RequestMethod.GET)
    public String listPage(Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        currentUser = (User) authentication.getPrincipal();
        model.addAttribute("allGroups", groupService.findByUser(userService.findByLogin(currentUser.getUsername())));
        List<Source> sources = sourceService.findByUser(userService.findByLogin(currentUser.getUsername()));
        model.addAttribute("allSources", sources);
        return "metrics/listPage";
    }

    @RequestMapping(value = {"/metrics/source/{sourceId}"}, method = RequestMethod.GET)
    public String listPageForSource(Model model, @PathVariable("sourceId") long source) {
        model.addAttribute("source", source);
        model.addAttribute("interval", 10);
        return "metrics/listPageForSource";
    }

    @RequestMapping(value = {"/metrics/group/{groupId}"}, method = RequestMethod.GET)
    public String listPageForGroup(Model model, @PathVariable("groupId") long group) {
        model.addAttribute("group", group);
        model.addAttribute("interval", 10);
        return "metrics/listPageForGroup";
    }

    @RequestMapping(value = {"/metrics/composite/interval/{interval}"}, method = RequestMethod.GET)
    @ResponseBody
    public String getAllLogsForIntervalByLevel(@PathVariable("interval") long interval) {
        StringBuilder result = new StringBuilder();
        Date date = new Date();
        result.append("{");
        for (Level level : Level.values()) {
            formatElementLineGraph(metricService.getLogCountForLevel(level, interval), level.toString(), result);
        }
        result.append("\"y\" : \"").append(new SimpleDateFormat("hh:mm:ss").format(date)).append("\"}");
        return result.toString();
    }

    @RequestMapping(value = {"/metrics/composite"}, method = RequestMethod.GET)
    @ResponseBody
    public String getAllLogsByLevel() {
        StringBuilder result = new StringBuilder();
        result.append("[");
        for (Level level : Level.values()) {
            formatElementDonutGraph(metricService.getLogCountForLevelTotal(level), level.toString(), result);
        }
        result.delete(result.length() - 1, result.length());
        result.append("]");
        return result.toString();
    }

    @RequestMapping(value = {"/metrics/source/composite/{source}/{interval}"}, method = RequestMethod.GET)
    @ResponseBody
    public String getAllLogsForSourceForIntervalByLevel(@PathVariable("source") long source, @PathVariable("interval") long interval) {
        StringBuilder result = new StringBuilder();
        Date date = new Date();
        result.append("{");
        for (Level level : Level.values()) {
            formatElementLineGraph(metricService.getLogCountForLevel(source, level, interval), level.toString(), result);
        }
        result.append("\"y\" : \"").append(new SimpleDateFormat("hh:mm:ss").format(date)).append("\"}");
        return result.toString();
    }

    @RequestMapping(value = {"/metrics/source/composite/{source}"}, method = RequestMethod.GET)
    @ResponseBody
    public String getAllLogsForSourceByLevel(@PathVariable("source") long source) {
        StringBuilder result = new StringBuilder();
        result.append("[");
        for (Level level : Level.values()) {
            formatElementDonutGraph(metricService.getLogCountForLevelTotal(source, level), level.toString(), result);
        }
        result.delete(result.length() - 1, result.length());
        result.append("]");
        return result.toString();
    }

    @RequestMapping(value = {"/metrics/group/composite/{group}/{interval}"}, method = RequestMethod.GET)
    @ResponseBody
    public String getAllLogsForGroupForIntervalByLevel(@PathVariable("group") long group, @PathVariable("interval") long interval) {
        StringBuilder result = new StringBuilder();
        Date date = new Date();
        result.append("{");
        for (Level level : Level.values()) {
            formatElementLineGraph(metricService.getLogCountForLevelForGroup(group, level, interval), level.toString(), result);
        }
        result.append("\"y\" : \"").append(new SimpleDateFormat("hh:mm:ss").format(date)).append("\"}");
        return result.toString();
    }

    @RequestMapping(value = {"/metrics/group/composite/{group}"}, method = RequestMethod.GET)
    @ResponseBody
    public String getAllLogsForGroupByLevelTotal(@PathVariable("group") long group) {
        StringBuilder result = new StringBuilder();
        result.append("[");
        for (Level level : Level.values()) {
            formatElementDonutGraph(metricService.getLogCountForLevelForGroupTotal(group, level), level.toString(), result);
        }
        result.delete(result.length() - 1, result.length());
        result.append("]");
        return result.toString();
    }

    private StringBuilder formatElementLineGraph(long value, String level, StringBuilder stringBuilder) {
        stringBuilder.append("\"").append(level).append("\" : \"").append(value).append("\",");
        return stringBuilder;
    }

    private StringBuilder formatElementDonutGraph(long value, String level, StringBuilder stringBuilder) {
        stringBuilder.append("{\"label\"").append(" : \"").append(level).append("\",").append("\"value\" : \"").append(value).append("\"},");
        return stringBuilder;
    }

}
