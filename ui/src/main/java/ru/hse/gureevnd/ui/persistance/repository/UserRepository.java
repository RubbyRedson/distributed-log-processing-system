package ru.hse.gureevnd.ui.persistance.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import ru.hse.gureevnd.ui.persistance.entity.User;

/**
 * Created by Nikita on 17.04.2016.
 */
@Repository
public interface UserRepository extends CrudRepository<User, Long> {
	User findByLogin(String login);
}
