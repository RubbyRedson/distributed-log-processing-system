package ru.hse.gureevnd.ui.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.hse.gureevnd.ui.converters.EntityConverter;
import ru.hse.gureevnd.ui.entities.Alert;
import ru.hse.gureevnd.ui.entities.Level;
import ru.hse.gureevnd.ui.entities.User;
import ru.hse.gureevnd.ui.persistance.entity.Source;
import ru.hse.gureevnd.ui.persistance.service.AlertService;
import ru.hse.gureevnd.ui.persistance.service.SourceService;
import ru.hse.gureevnd.ui.persistance.service.UserService;

import javax.validation.Valid;
import java.util.List;


@Controller
public class AlertController {

    @Autowired
    AlertService alertService;

    @Autowired
    UserService userService;

    @Autowired
    SourceService sourceService;

    @Autowired
    EntityConverter entityConverter;

    private User currentUser;

    @RequestMapping(value = {"/alerts"}, method = RequestMethod.GET)
    public String listPage(Model model) {
        listPageLogic(model);
        return "alerts/listPage";
    }

    @RequestMapping(value = {"/alerts"}, params = {"edit"}, method = RequestMethod.GET)
    public String listPageAfterEdit(Model model, @RequestParam(value = "edit") String edit) {
        listPageLogic(model);
        model.addAttribute("editAlert", edit);
        return "alerts/listPage";
    }

    @RequestMapping(value = {"/alerts"}, params = {"create"}, method = RequestMethod.GET)
    public String listPageAfterCreate(Model model, @RequestParam(value = "create") String create) {
        listPageLogic(model);
        model.addAttribute("createAlert", create);
        return "alerts/listPage";
    }

    @RequestMapping(value = {"/alerts"}, params = {"delete"}, method = RequestMethod.GET)
    public String listPageAfterDelete(Model model, @RequestParam String delete) {
        listPageLogic(model);
        model.addAttribute("deleteAlert", delete);
        return "alerts/listPage";
    }

    private void listPageLogic(Model model) {
        model.addAttribute("alert", new Alert());
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        currentUser = (User) authentication.getPrincipal();
        List<Source> sources = sourceService.findByUser(userService.findByLogin(currentUser.getUsername()));
        model.addAttribute("allSources", sources);
        model.addAttribute("allAlerts", alertService.findBySources(sources));
    }


    @RequestMapping(value = "/alert/{operation}/{alertId}", method = RequestMethod.GET)
    public String editRemoveAlert(@PathVariable("operation") String operation,
                                  @PathVariable("alertId") long alertId,
                                  Model model) {
        if (operation.equals("delete")) {
            String result = "fail";
            if (alertService.findById(alertId) != null) {
                alertService.delete(alertId);
                result = "success";
            }
            return "redirect:/alerts" + "?delete=" + result;
        } else if (operation.equals("edit")) {
            model.addAttribute("levels", Level.values());
            ru.hse.gureevnd.ui.persistance.entity.Alert repo = alertService.findById(alertId);
            Alert editAlert = entityConverter.convertFromEntityAlert(repo);
            if (editAlert != null) {
                model.addAttribute("editAlert", editAlert);
                Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
                currentUser = (User) authentication.getPrincipal();
                model.addAttribute("allSources", sourceService.findByUser(userService.findByLogin(currentUser.getUsername())));
                return "alerts/editPage";
            }
        }

        return "redirect:/alerts";
    }

    @RequestMapping(value = "/alert/update/{alertId}", method = RequestMethod.POST)
    public String updateAlert(Model model, @PathVariable("alertId") long alertId,
                              @ModelAttribute("editAlert") @Valid Alert editAlert, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            currentUser = (User) authentication.getPrincipal();
            List<Source> sources = sourceService.findByUser(userService.findByLogin(currentUser.getUsername()));
            model.addAttribute("editAlert", editAlert);
            model.addAttribute("allSources", sources);
            model.addAttribute("levels", Level.values());
            return "alerts/editPage";
        }
        editAlert.setId(alertId);
        String result = "fail";
        if (alertService.save(entityConverter.convertToEntityAlert(editAlert)) != null) {
            result = "success";
        }
        return "redirect:/alerts" + "?edit=" + result;
    }

    @RequestMapping(value = "/alert/create", method = RequestMethod.POST)
    public String createAlert(Model model, @ModelAttribute("alert") @Valid Alert alert, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            currentUser = (User) authentication.getPrincipal();
            List<Source> sources = sourceService.findByUser(userService.findByLogin(currentUser.getUsername()));
            model.addAttribute("sources", sources);
            model.addAttribute("levels", Level.values());
            return "alerts/createPage";
        }

        String result = "fail";
        if (alertService.save(entityConverter.convertToEntityAlert(alert)) != null) {
            result = "success";
        }
        return "redirect:/alerts" + "?create=" + result;
    }

    @RequestMapping(value = "/alert/create", method = RequestMethod.GET)
    public String createAlert(Model model) {
        model.addAttribute("alert", new Alert());
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        currentUser = (User) authentication.getPrincipal();
        List<Source> sources = sourceService.findByUser(userService.findByLogin(currentUser.getUsername()));
        model.addAttribute("sources", sources);
        model.addAttribute("levels", Level.values());
        return "alerts/createPage";
    }

    @RequestMapping(value = "/alert/create/{sourceId}", method = RequestMethod.GET)
    public String createAlert(Model model, @PathVariable("sourceId") long source) {
        Alert alert = new Alert();
        alert.setSource(source);
        model.addAttribute("alert", new Alert());
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        currentUser = (User) authentication.getPrincipal();
        List<Source> sources = sourceService.findByUser(userService.findByLogin(currentUser.getUsername()));
        model.addAttribute("sources", sources);
        model.addAttribute("levels", Level.values());
        return "alerts/createPage";
    }

    @RequestMapping(value = "/alert/{sourceId}/display", method = RequestMethod.GET)
    public String displayAlertsBySource(Model model, @PathVariable("sourceId") long source) {
        model.addAttribute("allAlertForSource", alertService.findBySource(source));
        model.addAttribute("sourceObj", sourceService.findById(source));
        model.addAttribute("source", source);
        return "alerts/listPageBySource";
    }
}
