package ru.hse.gureevnd.ui.persistance.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import ru.hse.gureevnd.ui.persistance.entity.Alert;
import ru.hse.gureevnd.ui.persistance.entity.Script;
import ru.hse.gureevnd.ui.persistance.entity.Source;

import java.util.List;

/**
 * Created by Nikita on 17.04.2016.
 */
@Repository
public interface ScriptRepository extends CrudRepository<Script, Long> {
	List<Script> findBySource(Source source);

	Script findById(long id);

	boolean deleteById(long id);

	List<Script> findBySourceIn(List<Source> sources);
}
