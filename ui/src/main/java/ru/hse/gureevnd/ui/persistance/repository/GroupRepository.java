package ru.hse.gureevnd.ui.persistance.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import ru.hse.gureevnd.ui.persistance.entity.Group;
import ru.hse.gureevnd.ui.persistance.entity.User;

import java.util.List;

/**
 * Created by Nikita on 17.04.2016.
 */
@Repository
public interface GroupRepository extends CrudRepository<Group, Long> {
	List<Group> findByUser(User user);

	Group findById(long id);

	boolean deleteById(long id);
}
