package ru.hse.gureevnd.ui.entities;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by Nikita on 17.04.2016.
 */
public class Alert {

    @NotNull
    private long source;
    @NotNull
    private long id;
    @NotEmpty
    @Size(min = 1, max = 100)
    private String name;
    @NotNull
    private Level level;
    @Size(max = 511)
    private String pattern;
    @NotEmpty
    @Email
    @Size(max = 255)
    private String email;
    @NotEmpty
    @Size(max = 255)
    private String topic;
    @NotEmpty
    @Size(max = 1023)
    private String template;

    public Alert() {
    }

    public Alert(long id, long source, String name, @NotEmpty String level, String pattern, String email, String topic, String template) {
        this.id = id;
        this.source = source;
        this.name = name;
        this.level = Level.valueOf(level.toUpperCase());
        this.pattern = pattern;
        this.email = email;
        this.topic = topic;
        this.template = template;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public long getSource() {
        return source;
    }

    public void setSource(long source) {
        this.source = source;
    }

    public Level getLevel() {
        return level;
    }

    public void setLevel(Level level) {
        this.level = level;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Alert{" +
                ", source=" + source +
                ", name='" + name + '\'' +
                ", level='" + level + '\'' +
                ", email='" + email + '\'' +
                ", topic='" + topic + '\'' +
                ", template='" + template + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Alert alert = (Alert) o;

        if (source != alert.source) return false;
        if (id != alert.id) return false;
        if (name != null ? !name.equals(alert.name) : alert.name != null) return false;
        if (level != alert.level) return false;
        if (pattern != null ? !pattern.equals(alert.pattern) : alert.pattern != null) return false;
        if (email != null ? !email.equals(alert.email) : alert.email != null) return false;
        if (topic != null ? !topic.equals(alert.topic) : alert.topic != null) return false;
        return template != null ? template.equals(alert.template) : alert.template == null;

    }

    @Override
    public int hashCode() {
        int result = (int) (source ^ (source >>> 32));
        result = 31 * result + (int) (id ^ (id >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (level != null ? level.hashCode() : 0);
        result = 31 * result + (pattern != null ? pattern.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (topic != null ? topic.hashCode() : 0);
        result = 31 * result + (template != null ? template.hashCode() : 0);
        return result;
    }
}
