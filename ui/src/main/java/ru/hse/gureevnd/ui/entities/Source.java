package ru.hse.gureevnd.ui.entities;

import org.hibernate.validator.constraints.NotEmpty;
import ru.hse.gureevnd.ui.entities.User;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * Created by Nikita on 17.04.2016.
 */
public class Source {

    private long id;
    private User user;

    @NotEmpty
    @Size(min = 1, max = 100)
    private String name;
    @Pattern(regexp="[0-9]+(?:\\.[0-9]+){3}(:[0-9]+)?", message = "Invalid ip address")
    private String ip;

    @Size(max = 100)
    private String login;
    @Size(max = 64)
    private String password;

    public Source() {
    }

    public Source(long id, User user, String name, String ip, String login, String password) {
        this.id = id;
        this.user = user;
        this.name = name;
        this.ip = ip;
        this.login = login;
        this.password = password;
    }

    public Source(String name, String ip, String login, String password) {
        this.user = user;
        this.name = name;
        this.ip = ip;
        this.login = login;
        this.password = password;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getIp() {
        return ip;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    @Override
    public String toString() {
        return "Source{" +
                ", user=" + user +
//				", groups=" + groups +
                ", name='" + name + '\'' +
                ", ip='" + ip + '\'' +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Source source = (Source) o;

        if (id != source.id) return false;
        if (user != null ? !user.equals(source.user) : source.user != null) return false;
        if (name != null ? !name.equals(source.name) : source.name != null) return false;
        if (ip != null ? !ip.equals(source.ip) : source.ip != null) return false;
        if (login != null ? !login.equals(source.login) : source.login != null) return false;
        return password != null ? password.equals(source.password) : source.password == null;

    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (user != null ? user.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (ip != null ? ip.hashCode() : 0);
        result = 31 * result + (login != null ? login.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        return result;
    }
}
