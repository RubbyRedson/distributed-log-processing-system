package ru.hse.gureevnd.ui.persistance.entity;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by Nick on 5/25/2016.
 */
@Entity
@Table(name = "logs")
public class Log {
    @Id
    @Column(name = "id", columnDefinition = "serial")
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    @JoinColumn(name="source_id", nullable=false)
    private Source source;

    private String text;
    private String level;
    private Timestamp time;

    public Log() {
    }

    public Log(Source source, String text, String level, Timestamp timestamp) {
        this.source = source;
        this.text = text;
        this.level = level;
        this.time = timestamp;
    }

    public Timestamp getTimestamp() {
        return time;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.time = timestamp;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Source getSource() {
        return source;
    }

    public void setSource(Source source) {
        this.source = source;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getString() {
        return level;
    }

    public void setString(String level) {
        this.level = level;
    }
}
