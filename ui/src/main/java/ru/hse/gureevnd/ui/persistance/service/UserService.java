package ru.hse.gureevnd.ui.persistance.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.hse.gureevnd.ui.persistance.entity.User;
import ru.hse.gureevnd.ui.persistance.entity.User;
import ru.hse.gureevnd.ui.persistance.repository.UserRepository;

import java.util.List;

/**
 * Created by Nikita on 17.04.2016.
 */
@Service
public class UserService {
	@Autowired
	UserRepository userRepository;

	public User findByLogin(String login) {
		return userRepository.findByLogin(login);
	}

	public Iterable<User> findAll() {
		return userRepository.findAll();
	}

	public User save(User user) {
		return userRepository.save(user);
	}

	public void delete(long id) {
		userRepository.delete(id);
	}

	public boolean existsUser(String login) {
		return userRepository.findByLogin(login) != null;
	}
}
