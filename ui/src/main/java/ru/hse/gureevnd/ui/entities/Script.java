package ru.hse.gureevnd.ui.entities;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by Nikita on 17.04.2016.
 */
public class Script {

    @NotNull
    private long source;
    @NotNull
    private long id;

    @NotEmpty
    @Size(min = 1, max = 100)
    private String name;
    @NotNull
    private Level level;
    @NotEmpty
    @Size(max = 511)
    private String path;
    @Size(max = 511)
    private String pattern;

    public Script() {
    }

    public Script(long source, String name, @NotNull String level, String path, String pattern) {
        this.source = source;
        this.name = name;
        this.level = Level.valueOf(level.toUpperCase());
        this.path = path;
        this.pattern = pattern;
    }

    public Script(long source, long id, String name, String level, String path, String pattern) {
        this.source = source;
        this.id = id;
        this.name = name;
        this.level = Level.valueOf(level.toUpperCase());
        this.path = path;
        this.pattern = pattern;
    }

    public long getSource() {
        return source;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setSource(long source) {
        this.source = source;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Level getLevel() {
        return level;
    }

    public void setLevel(Level level) {
        this.level = level;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    @Override
    public String toString() {
        return "Script{" +
                ", source=" + source +
                ", name='" + name + '\'' +
                ", level='" + level + '\'' +
                ", path='" + path + '\'' +
                ", pattern='" + pattern + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Script script = (Script) o;

        if (source != script.source) return false;
        if (id != script.id) return false;
        if (name != null ? !name.equals(script.name) : script.name != null) return false;
        if (level != script.level) return false;
        if (path != null ? !path.equals(script.path) : script.path != null) return false;
        return pattern != null ? pattern.equals(script.pattern) : script.pattern == null;

    }

    @Override
    public int hashCode() {
        int result = (int) (source ^ (source >>> 32));
        result = 31 * result + (int) (id ^ (id >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (level != null ? level.hashCode() : 0);
        result = 31 * result + (path != null ? path.hashCode() : 0);
        result = 31 * result + (pattern != null ? pattern.hashCode() : 0);
        return result;
    }
}
