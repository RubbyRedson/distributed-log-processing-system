package ru.hse.gureevnd.ui.persistance.entity;

import javax.persistence.*;

/**
 * Created by Nikita on 17.04.2016.
 */
@Entity
@Table(name="alerts")
public class Alert {
	@Id
	@Column(name = "id", columnDefinition = "serial")
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private long id;

	@ManyToOne
	@JoinColumn(name="source_id", nullable=false)
	private Source source;

	private String name;
	private String level;
	private String email;
	private String topic;
	private String pattern;
	private String template;

	public Alert() {
	}

	public Alert(Source source, String name, String level, String email, String topic, String template, String pattern) {
		this.source = source;
		this.name = name;
		this.level = level;
		this.email = email;
		this.topic = topic;
		this.template = template;
		this.pattern = pattern;
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Source getSource() {
		return source;
	}

	public void setSource(Source source) {
		this.source = source;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Alert{" +
				"id=" + id +
				", source=" + source +
				", name='" + name + '\'' +
				", level='" + level + '\'' +
				", email='" + email + '\'' +
				", topic='" + topic + '\'' +
				", template='" + template + '\'' +
				", pattern='" + pattern + '\'' +
				'}';
	}
}
