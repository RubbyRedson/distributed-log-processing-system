package ru.hse.gureevnd.ui.persistance.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.hse.gureevnd.ui.entities.Level;
import ru.hse.gureevnd.ui.persistance.repository.LogRepository;

/**
 * Created by Nick on 5/16/2016.
 */
@Service
public class MetricService {
    @Autowired
    LogRepository logRepository;

    @Autowired
    GroupService groupService;

    public long getLogCountForLevel(Level level, long seconds) {
        return logRepository.getLogCountForLevel(level.toString(), seconds);
    }

    public long getLogCountForLevelTotal(Level level) {
        return logRepository.getLogCountForLevelTotal(level.toString());
    }

    public long getLogCountForLevel(long source, Level level, long seconds) {
        return logRepository.getLogCountForLevel(source, level.toString(), seconds);
    }

    public long getLogCountForLevelTotal(long source, Level level) {
        return logRepository.getLogCountForLevelTotal(source, level.toString());
    }

    public long getLogCountForLevelForGroup(long groupId, Level level, long seconds) {
        ru.hse.gureevnd.ui.persistance.entity.Group group = groupService.findById(groupId);
        if (group == null || group.getSources() == null || group.getSources().isEmpty()) return 0;
        if (group.getSources() != null && !group.getSources().isEmpty()) {
            long[] sources = new long[group.getSources().size()];
            int i = 0;
            for (ru.hse.gureevnd.ui.persistance.entity.Source source : group.getSources()) {
                sources[i] = source.getId();
                i++;
            }
            return logRepository.getLogCountForLevelForGroup(sources, level.toString(), seconds);

        } else return 0;
    }

    public long getLogCountForLevelForGroupTotal(long groupId, Level level) {
        ru.hse.gureevnd.ui.persistance.entity.Group group = groupService.findById(groupId);
        if (group != null && group.getSources() != null && !group.getSources().isEmpty()) {
            long[] sources = new long[group.getSources().size()];
            int i = 0;
            for (ru.hse.gureevnd.ui.persistance.entity.Source source : group.getSources()) {
                sources[i] = source.getId();
                i++;
            }
            return logRepository.getLogCountForLevelTotal(sources, level.toString());

        } else return 0;
    }

}
