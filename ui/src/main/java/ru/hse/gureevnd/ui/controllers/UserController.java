package ru.hse.gureevnd.ui.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.hse.gureevnd.ui.converters.EntityConverter;
import ru.hse.gureevnd.ui.entities.User;
import ru.hse.gureevnd.ui.persistance.service.UserService;

import javax.validation.Valid;

/**
 * Created by Nick on 5/23/2016.
 */
@Controller
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    EntityConverter entityConverter;

    User currentUser;

    @RequestMapping(path = "/profile",
            method = RequestMethod.GET)
    public Object profile(Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        currentUser = (User) authentication.getPrincipal();
        model.addAttribute("user", currentUser);
        return "user/profile";
    }

    @RequestMapping(path = {"/user/update"},
            method = RequestMethod.POST)
    public Object updateUser(Model model, @ModelAttribute("user") @Valid User user, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("user", user);
            return "user/profile";
        }
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userService.save(entityConverter.convertToEntityUser(user));

        return "redirect:/metrics";

    }
}
