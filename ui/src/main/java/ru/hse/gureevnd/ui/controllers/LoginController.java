package ru.hse.gureevnd.ui.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Nikita on 09.05.2016.
 */
@Controller
public class LoginController {

    @RequestMapping(path = "/login",
            method = RequestMethod.GET)
    public Object login() {
        return "login";
    }

    @RequestMapping(path = {"/", "/index"},
            method = RequestMethod.GET)
    public Object index() {
        return "redirect:/metrics";
    }

}
