package ru.hse.gureevnd.ui.persistance.entity;

import javax.persistence.*;

/**
 * Created by Nikita on 17.04.2016.
 */
@Entity
@Table(name="scripts")
public class Script {
	@Id
	@Column(name = "id", columnDefinition = "serial")
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private long id;

	@ManyToOne
	@JoinColumn(name="source_id", nullable=false)
	private Source source;

	private String name;
	private String level;
	private String path;
	private String pattern;

	public Script() {
	}

	public Script(Source source, String name, String level, String path, String pattern) {
		this.source = source;
		this.name = name;
		this.level = level;
		this.path = path;
		this.pattern = pattern;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Source getSource() {
		return source;
	}

	public void setSource(Source source) {
		this.source = source;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	@Override
	public String toString() {
		return "Script{" +
				"id=" + id +
				", source=" + source +
				", name='" + name + '\'' +
				", level='" + level + '\'' +
				", path='" + path + '\'' +
				", pattern='" + pattern + '\'' +
				'}';
	}
}
