package ru.hse.gureevnd.ui.persistance.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.hse.gureevnd.ui.persistance.entity.Log;
import ru.hse.gureevnd.ui.persistance.entity.Source;

import java.util.List;

/**
 * Created by Nick on 5/16/2016.
 */
@Repository
public interface LogRepository extends CrudRepository<Log, Long>{

    @Query(value = "select COUNT(1) from logs where level = :level and time > DateADD(second, - :interval, Current_TimeStamp)", nativeQuery = true)
    long getLogCountForLevel(@Param("level") String level, @Param("interval") long seconds);

    @Query(value = "select COUNT(1) from logs where level = :level", nativeQuery = true)
    long getLogCountForLevelTotal(@Param("level") String level);

    @Query(value = "select COUNT(1) from logs where source_id = :source and level = :level and time > DateADD(second, - :interval, Current_TimeStamp)", nativeQuery = true)
    long getLogCountForLevel(@Param("source") long source, @Param("level") String level, @Param("interval") long seconds);

    @Query(value = "select COUNT(1) from logs where source_id = :source and level = :level", nativeQuery = true)
    long getLogCountForLevelTotal(@Param("source") long source, @Param("level") String level);

    @Query(value = "select COUNT(1) from logs where source_id in (:sources) and level = :level", nativeQuery = true)
    long getLogCountForLevelTotal(@Param("sources") long[] sources, @Param("level") String level);

    @Query(value = "select COUNT(1) from logs where source_id in (:sources) and level = :level and time > DateADD(second, - :interval, Current_TimeStamp)", nativeQuery = true)
    long getLogCountForLevelForGroup(@Param("sources") long[] sources, @Param("level") String level, @Param("interval") long seconds);
}
