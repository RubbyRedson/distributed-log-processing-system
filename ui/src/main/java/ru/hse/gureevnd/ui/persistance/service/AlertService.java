package ru.hse.gureevnd.ui.persistance.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.hse.gureevnd.ui.persistance.entity.Alert;
import ru.hse.gureevnd.ui.persistance.entity.Source;
import ru.hse.gureevnd.ui.persistance.repository.AlertRepository;

import java.util.List;

/**
 * Created by Nikita on 17.04.2016.
 */
@Service
public class AlertService {

	@Autowired
	AlertRepository alertRepository;

	@Autowired
	SourceService sourceService;

	public List<Alert> findBySource(Source source) {
		return alertRepository.findBySource(source);
	}

	public List<Alert> findBySource(long source) {
		return alertRepository.findBySource(sourceService.findById(source));
	}

	public List<Alert> findBySources(List<Source> sources) {
		return alertRepository.findBySourceIn(sources);
	}

	public Alert findById(long id) {
		return alertRepository.findById(id);
	}

	public boolean deleteById(long id) {
		return alertRepository.deleteById(id);
	}

	public Iterable<Alert> findAll() {
		return alertRepository.findAll();
	}

	public Alert save(Alert alert) {
		return alertRepository.save(alert);
	}

	public void delete(long id) {
		alertRepository.delete(id);
	}
}
