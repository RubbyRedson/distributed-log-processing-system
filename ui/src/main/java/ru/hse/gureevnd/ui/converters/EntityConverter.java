package ru.hse.gureevnd.ui.converters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.hse.gureevnd.ui.entities.*;
import ru.hse.gureevnd.ui.entities.Alert;
import ru.hse.gureevnd.ui.entities.Group;
import ru.hse.gureevnd.ui.entities.Script;
import ru.hse.gureevnd.ui.entities.Source;
import ru.hse.gureevnd.ui.entities.User;
import ru.hse.gureevnd.ui.persistance.entity.*;
import ru.hse.gureevnd.ui.persistance.service.*;

import javax.validation.constraints.NotNull;
import java.util.*;

/**
 * Created by Nikita on 03.05.2016.
 */
@Controller
public class EntityConverter {

    @Autowired
    AlertService alertService;

    @Autowired
    SourceService sourceService;

    @Autowired
    ScriptService scriptService;

    @Autowired
    GroupService groupService;

    @Autowired
    UserService userService;

    public ru.hse.gureevnd.ui.persistance.entity.Alert convertToEntityAlert(@NotNull Alert a) {
        ru.hse.gureevnd.ui.persistance.entity.Alert result = alertService.findById(a.getId());
        if (result == null)
            result = new ru.hse.gureevnd.ui.persistance.entity.Alert();
        result.setSource(sourceService.findById(a.getSource()));
        result.setId(a.getId());
        result.setName(a.getName());
        result.setEmail(a.getEmail());
        if (a.getLevel() != null)
            result.setLevel(a.getLevel().name());
        result.setTemplate(a.getTemplate());
        result.setTopic(a.getTopic());
        result.setPattern(a.getPattern());
        return result;
    }

    public ru.hse.gureevnd.ui.persistance.entity.Script convertToEntityScript(@NotNull Script a) {
        ru.hse.gureevnd.ui.persistance.entity.Script result = scriptService.findById(a.getId());
        if (result == null)
            result = new ru.hse.gureevnd.ui.persistance.entity.Script();
        result.setSource(sourceService.findById(a.getSource()));
        result.setName(a.getName());
        result.setPath(a.getPath());
        if (a.getLevel() != null)
            result.setLevel(a.getLevel().name());
        result.setPattern(a.getPattern());
        return result;
    }

    public ru.hse.gureevnd.ui.persistance.entity.Source convertToEntitySource(@NotNull Source s) {
        ru.hse.gureevnd.ui.persistance.entity.Source result = sourceService.findById(s.getId());
        if (result == null)
            result = new ru.hse.gureevnd.ui.persistance.entity.Source();
        if (s.getUser() != null)
            result.setUser(userService.findByLogin(s.getUser().getUsername()));
        result.setName(s.getName());
        result.setIp(s.getIp());
        result.setLogin(s.getLogin());
        result.setPassword(s.getPassword());
        return result;
    }

    public ru.hse.gureevnd.ui.persistance.entity.Group convertToEntityGroup(@NotNull Group g) {
        ru.hse.gureevnd.ui.persistance.entity.Group result = groupService.findById(g.getId());
        if (result == null)
            result = new ru.hse.gureevnd.ui.persistance.entity.Group();
        if (g.getUser() != null)
            result.setUser(userService.findByLogin(g.getUser().getUsername()));
        result.setName(g.getName());
        Set<ru.hse.gureevnd.ui.persistance.entity.Source> set = new HashSet<>();
        for (Source s : g.getSources())
            set.add(convertToEntitySource(s));
        result.setSources(set);
        return result;
    }


    public Source convertFromEntitySource(@NotNull ru.hse.gureevnd.ui.persistance.entity.Source s) {
        Source result = new Source();
        result.setId(s.getId());
        result.setUser(new User(s.getUser()));
        result.setName(s.getName());
        result.setIp(s.getIp());
        result.setLogin(s.getLogin());
        result.setPassword(s.getPassword());
        return result;
    }

    public Group convertFromEntityGroup(@NotNull ru.hse.gureevnd.ui.persistance.entity.Group g) {
        Group result = new Group();
        result.setId(g.getId());
        result.setUser(new User(g.getUser()));
        result.setName(g.getName());
        if (g.getSources() != null) {
            int count = g.getSources().size();
            Source[] arr = new Source[count];
            int i = 0;
            for (ru.hse.gureevnd.ui.persistance.entity.Source s : g.getSources()) {
                arr[i] = convertFromEntitySource(s);
                i++;
            }
            result.setSources(arr);
        } else {
            result.setSources(new Source[0]);
        }
        return result;
    }

    public Alert convertFromEntityAlert(@NotNull ru.hse.gureevnd.ui.persistance.entity.Alert a) {
        Alert result = new Alert();
        if (a == null)
            return result;
        result.setSource(a.getSource() == null ? 1 : a.getSource().getId());
        result.setId(a.getId());
        result.setName(a.getName());
        result.setEmail(a.getEmail());
        if (a.getLevel() != null)
            result.setLevel(Level.valueOf(a.getLevel().toUpperCase()));
        result.setTemplate(a.getTemplate());
        result.setTopic(a.getTopic());
        result.setPattern(a.getPattern());
        return result;
    }

    public Script convertFromEntityScript(@NotNull ru.hse.gureevnd.ui.persistance.entity.Script s) {
        Script result = new Script();
        result.setId(s.getId());
        result.setSource(s.getSource() == null ? 1 : s.getSource().getId());
        result.setName(s.getName());
        result.setPath(s.getPath());
        if (s.getLevel() != null)
            result.setLevel(Level.valueOf(s.getLevel().toUpperCase()));
        result.setPattern(s.getPattern());
        return result;
    }

    public List<Source> convertSetFromEntitySource(@NotNull List<ru.hse.gureevnd.ui.persistance.entity.Source> sources) {
        List<Source> result = new ArrayList<>();
        for (ru.hse.gureevnd.ui.persistance.entity.Source source : sources)
            result.add(convertFromEntitySource(source));
        return result;
    }

    public ru.hse.gureevnd.ui.persistance.entity.User convertToEntityUser(@NotNull User user) {
        ru.hse.gureevnd.ui.persistance.entity.User result = new ru.hse.gureevnd.ui.persistance.entity.User();
        result.setLogin(user.getUsername());
        result.setPassword(user.getPassword());
        return result;
    }

}
