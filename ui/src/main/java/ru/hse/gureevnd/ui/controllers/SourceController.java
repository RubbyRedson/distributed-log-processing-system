package ru.hse.gureevnd.ui.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.hse.gureevnd.ui.converters.EntityConverter;
import ru.hse.gureevnd.ui.entities.Source;
import ru.hse.gureevnd.ui.entities.User;
import ru.hse.gureevnd.ui.persistance.service.SourceService;
import ru.hse.gureevnd.ui.persistance.service.UserService;

import javax.validation.Valid;


@Controller
public class SourceController {

    @Autowired
    UserService userService;

    @Autowired
    SourceService sourceService;

    @Autowired
    EntityConverter entityConverter;

    private User currentUser;

    @RequestMapping(value = {"/sources"}, method = RequestMethod.GET)
    public String listPage(Model model) {
        listPageLogic(model);
        return "sources/listPage";
    }

    @RequestMapping(value = {"/sources"}, params = {"edit"}, method = RequestMethod.GET)
    public String listPageAfterEdit(Model model, @RequestParam(value = "edit") String edit) {
        listPageLogic(model);
        model.addAttribute("editSource", edit);
        return "sources/listPage";
    }

    @RequestMapping(value = {"/sources"}, params = {"create"}, method = RequestMethod.GET)
    public String listPageAfterCreate(Model model, @RequestParam(value = "create") String create) {
        listPageLogic(model);
        model.addAttribute("createSource", create);
        return "sources/listPage";
    }

    @RequestMapping(value = {"/sources"}, params = {"delete"}, method = RequestMethod.GET)
    public String listPageAfterDelete(Model model, @RequestParam String delete) {
        listPageLogic(model);
        model.addAttribute("deleteSource", delete);
        return "sources/listPage";
    }

    private void listPageLogic(Model model) {
        model.addAttribute("source", new Source());
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        currentUser = (User) authentication.getPrincipal();
        model.addAttribute("allSources", sourceService.findByUser(userService.findByLogin(currentUser.getUsername())));
    }


    @RequestMapping(value = "/source/{operation}/{sourceId}", method = RequestMethod.GET)
    public String editRemoveSource(@PathVariable("operation") String operation,
                                   @PathVariable("sourceId") long sourceId,
                                   Model model) {
        if (operation.equals("delete")) {
            String result = "fail";
            if (sourceService.findById(sourceId) != null) {
                sourceService.delete(sourceId);
                result = "success";
            }
            return "redirect:/sources" + "?delete=" + result;
        } else if (operation.equals("edit")) {
            ru.hse.gureevnd.ui.persistance.entity.Source repo = sourceService.findById(sourceId);
            Source editSource = entityConverter.convertFromEntitySource(repo);
            editSource.setId(sourceId);
            model.addAttribute("editSource", editSource);
            return "sources/editPage";
        }

        return "redirect:/sources";
    }

    @RequestMapping(value = "/source/update/{sourceId}", method = RequestMethod.POST)
    public String updateSource(@ModelAttribute("editSource") @Valid Source editSource, BindingResult bindingResult,
                               @PathVariable("sourceId") long sourceId, Model model) {

        editSource.setUser(currentUser);
        editSource.setId(sourceId);

        if (bindingResult.hasErrors()) {
            editSource.setId(sourceId);
            model.addAttribute("editSource", editSource);
            return "sources/editPage";
        }

        String result = "fail";
        if (sourceService.save(entityConverter.convertToEntitySource(editSource)) != null) {
            result = "success";
        }
        return "redirect:/sources" + "?edit=" + result;
    }

    @RequestMapping(value = "/source/create", method = RequestMethod.POST)
    public String createSource(@ModelAttribute("source") @Valid Source source, BindingResult bindingResult, Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        currentUser = (User) authentication.getPrincipal();
        source.setUser(currentUser);

        if (bindingResult.hasErrors()) {
            model.addAttribute("source", source);
            return "sources/createPage";
        }


        String result = "fail";
        if (sourceService.save(entityConverter.convertToEntitySource(source)) != null) {
            result = "success";
        }
        return "redirect:/sources" + "?create=" + result;
    }

    @RequestMapping(value = "/source/create", method = RequestMethod.GET)
    public String createSource(Model model) {
        Source s = new Source();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        currentUser = (User) authentication.getPrincipal();
        s.setUser(currentUser);
        model.addAttribute("source", s);
        return "sources/createPage";
    }
}
