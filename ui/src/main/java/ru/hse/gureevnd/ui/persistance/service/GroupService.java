package ru.hse.gureevnd.ui.persistance.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.hse.gureevnd.ui.persistance.entity.Group;
import ru.hse.gureevnd.ui.persistance.entity.Group;
import ru.hse.gureevnd.ui.persistance.entity.Group;
import ru.hse.gureevnd.ui.persistance.entity.User;
import ru.hse.gureevnd.ui.persistance.repository.GroupRepository;

import java.util.List;

/**
 * Created by Nikita on 17.04.2016.
 */
@Service
public class GroupService {

	@Autowired
	GroupRepository groupRepository;

	public List<Group> findByUser(User user) {
		return groupRepository.findByUser(user);
	}

	public Group findById(long id) {
		return groupRepository.findById(id);
	}

	public boolean deleteById(long id) {
		return groupRepository.deleteById(id);
	}

	public Iterable<Group> findAll() {
		return groupRepository.findAll();
	}

	public Group save(Group group) {
		return groupRepository.save(group);
	}

	public void delete(long id) {
		groupRepository.delete(id);
	}
}
