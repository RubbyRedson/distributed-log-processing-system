package ru.hse.gureevnd.ui.persistance.entity;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by Nikita on 17.04.2016.
 */
@Entity
@Table(name="users")
public class User {
	@Id
	@Column(name = "id", columnDefinition = "serial")
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private long id;

	@Column(name="login", unique=true)
	private String login;
	@Column(name="password")
	private String password;

	@OneToMany(mappedBy = "user", fetch = FetchType.EAGER)
	private Set<Source> sources;

	@OneToMany(mappedBy = "user", fetch = FetchType.EAGER)
	private Set<Group> groups;

	public User() {
	}

	public User(String login, String password) {
		this.login = login;
		this.password = password;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof User)) return false;

		User user = (User) o;

		if (id != user.id) return false;
		if (login != null ? !login.equals(user.login) : user.login != null) return false;
		return password != null ? password.equals(user.password) : user.password == null;

	}

	@Override
	public int hashCode() {
		int result = (int) (id ^ (id >>> 32));
		result = 31 * result + (login != null ? login.hashCode() : 0);
		result = 31 * result + (password != null ? password.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "User{" +
				"id=" + id +
				", login='" + login + '\'' +
				", password='" + password + '\'' +
//				", sources=" + sources +
//				", groups=" + groups +
				'}';
	}
}
