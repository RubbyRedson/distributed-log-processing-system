package ru.hse.gureevnd.ui;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ConversionServiceFactoryBean;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import ru.hse.gureevnd.ui.converters.StringToSourceEntityConverter;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Nick on 5/22/2016.
 */
@Configuration
public class ConversionConfig {

    @Autowired
    StringToSourceEntityConverter stringToSourceEntityConverter;

    @Bean
    public ConversionService getConversionService() {
        ConversionServiceFactoryBean bean = new ConversionServiceFactoryBean();
        bean.setConverters(getConverters());
        bean.afterPropertiesSet();
        return bean.getObject();
    }

    private Set<Converter> getConverters() {
        Set<Converter> converters = new HashSet<>();

        converters.add(stringToSourceEntityConverter);

        return converters;
    }

}
