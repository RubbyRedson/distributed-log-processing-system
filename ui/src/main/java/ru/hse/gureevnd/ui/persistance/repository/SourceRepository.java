package ru.hse.gureevnd.ui.persistance.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import ru.hse.gureevnd.ui.persistance.entity.Source;
import ru.hse.gureevnd.ui.persistance.entity.User;

import java.util.List;

/**
 * Created by Nikita on 17.04.2016.
 */
@Repository
public interface SourceRepository extends CrudRepository<Source, Long> {
	List<Source> findByUser(User user);

	Source findById(long id);

	boolean deleteById(long id);
}
