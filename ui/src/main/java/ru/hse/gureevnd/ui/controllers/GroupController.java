package ru.hse.gureevnd.ui.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.hse.gureevnd.ui.converters.EntityConverter;
import ru.hse.gureevnd.ui.entities.Group;
import ru.hse.gureevnd.ui.entities.User;
import ru.hse.gureevnd.ui.persistance.service.GroupService;
import ru.hse.gureevnd.ui.persistance.service.SourceService;
import ru.hse.gureevnd.ui.persistance.service.UserService;

import javax.validation.Valid;


@Controller
public class GroupController {

    @Autowired
    UserService userService;

    @Autowired
    GroupService groupService;

    @Autowired
    SourceService sourceService;

    @Autowired
    EntityConverter entityConverter;

    private User currentUser;

    @RequestMapping(value = {"/groups"}, method = RequestMethod.GET)
    public String listPage(Model model) {
        listPageLogic(model);
        return "groups/listPage";
    }

    @RequestMapping(value = {"/groups"}, params = {"edit"}, method = RequestMethod.GET)
    public String listPageAfterEdit(Model model, @RequestParam(value = "edit") String edit) {
        listPageLogic(model);
        model.addAttribute("editGroup", edit);
        return "groups/listPage";
    }

    @RequestMapping(value = {"/groups"}, params = {"create"}, method = RequestMethod.GET)
    public String listPageAfterCreate(Model model, @RequestParam(value = "create") String create) {
        listPageLogic(model);
        model.addAttribute("createGroup", create);
        return "groups/listPage";
    }

    @RequestMapping(value = {"/groups"}, params = {"delete"}, method = RequestMethod.GET)
    public String listPageAfterDelete(Model model, @RequestParam String delete) {
        listPageLogic(model);
        model.addAttribute("deleteGroup", delete);
        return "groups/listPage";
    }

    private void listPageLogic(Model model) {
        model.addAttribute("group", new Group());
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        currentUser = (User) authentication.getPrincipal();
        model.addAttribute("allGroups", groupService.findByUser(userService.findByLogin(currentUser.getUsername())));
    }

    @RequestMapping(value = "/group/{operation}/{groupId}", method = RequestMethod.GET)
    public String editRemoveGroup(@PathVariable("operation") String operation,
                                  @PathVariable("groupId") long groupId,
                                  Model model) {
        if (operation.equals("delete")) {
            String result = "fail";
            if (groupService.findById(groupId) != null) {
                groupService.delete(groupId);
                result = "success";
            }
            return "redirect:/groups"  + "?delete=" + result;
        } else if (operation.equals("edit")) {
            ru.hse.gureevnd.ui.persistance.entity.Group repo = groupService.findById(groupId);
            Group editGroup = entityConverter.convertFromEntityGroup(repo);
            editGroup.setId(groupId);
            model.addAttribute("editGroup", editGroup);
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            currentUser = (User) authentication.getPrincipal();
            editGroup.setUser(currentUser);
            model.addAttribute("allSources", sourceService.findByUser(userService.findByLogin(currentUser.getUsername())));
            return "groups/editPage";
        }

        return "redirect:/groups";
    }

    @RequestMapping(value = "/group/update/{groupId}", method = RequestMethod.POST)
    public String updateGroup(@ModelAttribute("editGroup") @Valid Group editGroup, BindingResult bindingResult,
                              @PathVariable("groupId") long groupId, Model model) {
        if (bindingResult.hasErrors()) {
            editGroup.setId(groupId);
            model.addAttribute("editGroup", editGroup);
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            currentUser = (User) authentication.getPrincipal();
            editGroup.setUser(currentUser);
            model.addAttribute("allSources", sourceService.findByUser(userService.findByLogin(currentUser.getUsername())));
            return "groups/editPage";
        }

        editGroup.setId(groupId);
        editGroup.setUser(currentUser);
        String result = "fail";
        if (groupService.save(entityConverter.convertToEntityGroup(editGroup)) != null) {
            result = "success";
        }
        return "redirect:/groups"  + "?edit=" + result;
    }

    @RequestMapping(value = "/group/create", method = RequestMethod.POST)
    public String createGroup(@ModelAttribute("group") @Valid Group group, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            currentUser = (User) authentication.getPrincipal();
            group.setUser(currentUser);
            model.addAttribute("group", group);
            model.addAttribute("allSources", entityConverter.convertSetFromEntitySource(
                    sourceService.findByUser(userService.findByLogin(currentUser.getUsername()))));
            return "groups/createPage";
        }
        group.setUser(currentUser);
        String result = "fail";
        if (groupService.save(entityConverter.convertToEntityGroup(group)) != null) {
            result = "success";
        }
        return "redirect:/groups"  + "?create=" + result;
    }

    @RequestMapping(value = "/group/create", method = RequestMethod.GET)
    public String createGroup(Model model) {
        Group g = new Group();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        currentUser = (User) authentication.getPrincipal();
        g.setUser(currentUser);
        model.addAttribute("group", g);
        model.addAttribute("allSources", entityConverter.convertSetFromEntitySource(
                sourceService.findByUser(userService.findByLogin(currentUser.getUsername()))));
        return "groups/createPage";
    }
}
