package ru.hse.gureevnd.ui.entities;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * Created by Nikita on 17.04.2016.
 */
public class Group {

    @NotNull
    private long id;
    
    private User user;

    @NotEmpty
    @Size(min = 1, max = 100)
    private String name;

    @NotNull
    private Source[] sources = null;

    public Group() {
    }

    public Group(long id, User user, String name, Source[] sources) {
        this.id = id;
        this.user = user;
        this.name = name;
        this.sources = sources;
    }

    public Group(User user, String name, Source[] sources) {
        this.user = user;
        this.name = name;
        this.sources = sources;
    }


    public Source[] getSources() {
        return sources;
    }

    public void setSources(Source[] sources) {
        this.sources = sources;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Group{" +
                ", user=" + user +
                ", name='" + name + '\'' +
//				", sources=" + sources +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Group group = (Group) o;

        if (id != group.id) return false;
        if (user != null ? !user.equals(group.user) : group.user != null) return false;
        if (name != null ? !name.equals(group.name) : group.name != null) return false;
        return sources != null ? sources.equals(group.sources) : group.sources == null;

    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (user != null ? user.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (sources != null ? sources.hashCode() : 0);
        return result;
    }

    public boolean hasSource(long id) {
        if (sources == null) return false;
        for (Source source : sources) {
            if (source != null && id == source.getId())
                return true;
        }
        return false;
    }
}
