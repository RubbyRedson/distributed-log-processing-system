package ru.hse.gureevnd.ui.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.hse.gureevnd.ui.converters.EntityConverter;
import ru.hse.gureevnd.ui.entities.User;
import ru.hse.gureevnd.ui.persistance.service.UserService;

import javax.validation.Valid;

/**
 * Created by Nick on 5/23/2016.
 */
@Controller
public class RegistrationController {

    @Autowired
    UserService userService;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    EntityConverter entityConverter;

    @RequestMapping(path = "/register",
            method = RequestMethod.GET)
    public Object registerGet(Model model) {
        model.addAttribute("user", new User());
        return "registration";
    }

    @RequestMapping(path = {"/register"},
            method = RequestMethod.POST)
    public Object registerPost(Model model, @ModelAttribute("user") @Valid User user, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            model.addAttribute("user", user);
            return "registration";
        }
        if (userService.existsUser(user.getUsername())) {
            model.addAttribute("user", user);
            bindingResult.rejectValue("username", "error.username", "An account already exists for this username.");
            return "registration";
        }

        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userService.save(entityConverter.convertToEntityUser(user));

        return "redirect:/login";
    }
}
