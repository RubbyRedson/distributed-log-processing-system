package ru.hse.gureevnd.ui.entities;

/**
 * Created by Nick on 5/16/2016.
 */
public enum Level {
    INFO, DEBUG, WARN, ERROR, FATAL
}
