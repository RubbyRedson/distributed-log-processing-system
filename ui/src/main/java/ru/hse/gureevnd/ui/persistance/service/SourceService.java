package ru.hse.gureevnd.ui.persistance.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.hse.gureevnd.ui.persistance.entity.Source;
import ru.hse.gureevnd.ui.persistance.entity.Source;
import ru.hse.gureevnd.ui.persistance.entity.User;
import ru.hse.gureevnd.ui.persistance.repository.SourceRepository;
import ru.hse.gureevnd.ui.persistance.repository.UserRepository;

import java.util.List;

/**
 * Created by Nikita on 17.04.2016.
 */
@Service
public class SourceService {

	@Autowired
	SourceRepository sourceRepository;

	public List<Source> findByUser(User user) {
		return sourceRepository.findByUser(user);
	}

	public Source findById(long id) {
		return sourceRepository.findById(id);
	}

	public boolean deleteById(long id) {
		return sourceRepository.deleteById(id);
	}

	public Iterable<Source> findAll() {
		return sourceRepository.findAll();
	}

	public Source save(Source source) {
		return sourceRepository.save(source);
	}

	public void delete(long id) {
		sourceRepository.delete(id);
	}
}
