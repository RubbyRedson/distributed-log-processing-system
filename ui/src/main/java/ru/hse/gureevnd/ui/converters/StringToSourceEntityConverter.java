package ru.hse.gureevnd.ui.converters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Controller;
import ru.hse.gureevnd.ui.entities.Source;
import ru.hse.gureevnd.ui.persistance.service.SourceService;


/**
 * Created by Nick on 5/22/2016.
 */
@Controller
public class StringToSourceEntityConverter implements Converter<String, Source> {

    @Autowired
    SourceService sourceService;

    @Autowired
    EntityConverter entityConverter;

    @Override
    public Source convert(String s) {
        long id = Long.parseLong(s);
        Source res = entityConverter.convertFromEntitySource(sourceService.findById(id));
        return res;
    }
}
