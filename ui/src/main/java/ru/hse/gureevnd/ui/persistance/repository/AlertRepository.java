package ru.hse.gureevnd.ui.persistance.repository;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import ru.hse.gureevnd.ui.persistance.entity.Alert;
import ru.hse.gureevnd.ui.persistance.entity.Source;

import java.util.List;

/**
 * Created by Nikita on 17.04.2016.
 */
@Repository
public interface AlertRepository extends CrudRepository<Alert, Long> {
	List<Alert> findBySource(Source source);

	Alert findById(long id);

	boolean deleteById(long id);

	List<Alert> findBySourceIn(List<Source> sources);

}
