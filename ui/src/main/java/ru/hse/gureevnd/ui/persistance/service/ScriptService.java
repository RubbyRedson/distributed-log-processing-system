package ru.hse.gureevnd.ui.persistance.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.hse.gureevnd.ui.persistance.entity.Alert;
import ru.hse.gureevnd.ui.persistance.entity.Source;
import ru.hse.gureevnd.ui.persistance.entity.Script;
import ru.hse.gureevnd.ui.persistance.entity.Source;
import ru.hse.gureevnd.ui.persistance.repository.ScriptRepository;

import java.util.List;

/**
 * Created by Nikita on 17.04.2016.
 */
@Service
public class ScriptService {

	@Autowired
	ScriptRepository scriptRepository;

	@Autowired
	SourceService sourceService;

	public List<Script> findBySource(Source source) {
		return scriptRepository.findBySource(source);
	}

	public List<Script> findBySource(long source) {
		return scriptRepository.findBySource(sourceService.findById(source));
	}
	public List<Script> findBySources(List<Source> sources) {
		return scriptRepository.findBySourceIn(sources);
	}


	public Script findById(long id) {
		return scriptRepository.findById(id);
	}

	public boolean deleteById(long id) {
		return scriptRepository.deleteById(id);
	}

	public Iterable<Script> findAll() {
		return scriptRepository.findAll();
	}

	public Script save(Script script) {
		return scriptRepository.save(script);
	}

	public void delete(long id) {
		scriptRepository.delete(id);
	}
}
