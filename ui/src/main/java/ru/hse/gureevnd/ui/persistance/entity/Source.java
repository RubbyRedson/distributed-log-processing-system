package ru.hse.gureevnd.ui.persistance.entity;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by Nikita on 17.04.2016.
 */
@Entity
@Table(name = "sources")
public class Source {
	@Id
	@Column(name = "id", columnDefinition = "serial")
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private long id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="user_id", nullable=false)
	private User user;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(
			name = "Source_Groups",
			joinColumns = @JoinColumn(name = "source_id"),
			inverseJoinColumns = @JoinColumn(name = "group_id"))
	private Set<Group> groups;

	@OneToMany(cascade=CascadeType.REMOVE, mappedBy = "source")
	private Set<Alert> alerts;

	@OneToMany(cascade=CascadeType.REMOVE, mappedBy = "source")
	private Set<Script> scripts;

	@OneToMany(cascade=CascadeType.REMOVE, mappedBy = "source")
	private Set<Log> logs;

	private String name;
	private String ip;
	private String login;
	private String password;

	public Source() {
	}

	public Source(User user, String name, String ip, String login, String password) {
		this.user = user;
		this.name = name;
		this.ip = ip;
		this.login = login;
		this.password = password;
	}

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getIp() {
		return ip;
	}

	public String getLogin() {
		return login;
	}

	public String getPassword() {
		return password;
	}

	public void setId(long id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Set<Group> getGroups() {
		return groups;
	}

	public void setGroups(Set<Group> groups) {
		this.groups = groups;
	}

	public Set<Alert> getAlerts() {
		return alerts;
	}

	public void setAlerts(Set<Alert> alerts) {
		this.alerts = alerts;
	}

	public Set<Script> getScripts() {
		return scripts;
	}

	public void setScripts(Set<Script> scripts) {
		this.scripts = scripts;
	}

	public Set<Log> getLogs() {
		return logs;
	}

	public void setLogs(Set<Log> logs) {
		this.logs = logs;
	}

	@Override
	public String toString() {
		return "Source{" +
				"id=" + id +
				", user=" + user +
//				", groups=" + groups +
				", name='" + name + '\'' +
				", ip='" + ip + '\'' +
				", login='" + login + '\'' +
				", password='" + password + '\'' +
				'}';
	}
}
