package ru.hse.gureevnd.ui.persistance.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Nikita on 17.04.2016.
 */
@Entity
@Table(name = "groups")
public class Group {
	@Id
	@Column(name = "id", columnDefinition = "serial")
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private long id;

	public Group() {
	}

	public Group(User user, String name, Set<Source> sources) {
		this.user = user;
		this.name = name;
		this.sources = sources;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="user_id", nullable=false)
	private User user;

	private String name;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(
			name = "Source_Groups",
			joinColumns = @JoinColumn(name = "group_id"),
			inverseJoinColumns = @JoinColumn(name = "source_id"))
	private Set<Source> sources = new HashSet<>();

	public Set<Source> getSources() {
		return sources;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Group{" +
				"id=" + id +
				", user=" + user +
				", name='" + name + '\'' +
//				", sources=" + sources +
				'}';
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setSources(Set<Source> sources) {
		this.sources = sources;
	}
}
