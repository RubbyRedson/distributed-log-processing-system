package ru.hse.gureevnd.ui.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.hse.gureevnd.ui.converters.EntityConverter;
import ru.hse.gureevnd.ui.entities.*;
import ru.hse.gureevnd.ui.entities.Script;
import ru.hse.gureevnd.ui.entities.User;
import ru.hse.gureevnd.ui.persistance.service.ScriptService;
import ru.hse.gureevnd.ui.persistance.service.SourceService;
import ru.hse.gureevnd.ui.persistance.service.UserService;

import javax.validation.Valid;
import java.util.List;


@Controller
public class ScriptController {

    @Autowired
    ScriptService scriptService;

    @Autowired
    UserService userService;

    @Autowired
    SourceService sourceService;

    @Autowired
    EntityConverter entityConverter;

    private User currentUser;

    @RequestMapping(value = {"/scripts"}, method = RequestMethod.GET)
    public String listPage(Model model) {
        listPageLogic(model);
        return "scripts/listPage";
    }

    @RequestMapping(value = {"/scripts"}, params = {"edit"}, method = RequestMethod.GET)
    public String listPageAfterEdit(Model model, @RequestParam(value = "edit") String edit) {
        listPageLogic(model);
        model.addAttribute("editScript", edit);
        return "scripts/listPage";
    }

    @RequestMapping(value = {"/scripts"}, params = {"create"}, method = RequestMethod.GET)
    public String listPageAfterCreate(Model model, @RequestParam(value = "create") String create) {
        listPageLogic(model);
        model.addAttribute("createScript", create);
        return "scripts/listPage";
    }

    @RequestMapping(value = {"/scripts"}, params = {"delete"}, method = RequestMethod.GET)
    public String listPageAfterDelete(Model model, @RequestParam String delete) {
        listPageLogic(model);
        model.addAttribute("deleteScript", delete);
        return "scripts/listPage";
    }

    private void listPageLogic(Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        currentUser = (User) authentication.getPrincipal();
        List<ru.hse.gureevnd.ui.persistance.entity.Source> sources = sourceService.findByUser(userService.findByLogin(currentUser.getUsername()));
        model.addAttribute("allSources", sources);
        model.addAttribute("allScripts", scriptService.findAll());
    }

    @RequestMapping(value = "/script/{operation}/{scriptId}", method = RequestMethod.GET)
    public String editRemoveScript(@PathVariable("operation") String operation,
                                   @PathVariable("scriptId") long scriptId,
                                   Model model) {
        if (operation.equals("delete")) {
            String result = "fail";
            if (scriptService.findById(scriptId) != null) {
                scriptService.delete(scriptId);
                result = "success";
            }
            return "redirect:/scripts" + "?delete=" + result;
        } else if (operation.equals("edit")) {
            model.addAttribute("levels", Level.values());
            ru.hse.gureevnd.ui.persistance.entity.Script repo = scriptService.findById(scriptId);
            Script editScript = entityConverter.convertFromEntityScript(repo);
            editScript.setId(scriptId);
            model.addAttribute("editScript", editScript);
            model.addAttribute("sources", sourceService.findByUser(userService.findByLogin(currentUser.getUsername())));
            model.addAttribute("levels", Level.values());
            return "scripts/editPage";
        }

        return "redirect:/scripts";
    }

    @RequestMapping(value = "/script/update/{scriptId}", method = RequestMethod.POST)
    public String updateScript(@ModelAttribute("editScript") @Valid Script editScript, BindingResult bindingResult,
                               @PathVariable("scriptId") long scriptId, Model model) {

        if (bindingResult.hasErrors()) {
            model.addAttribute("levels", Level.values());
            editScript.setId(scriptId);
            model.addAttribute("editScript", editScript);
            model.addAttribute("sources", sourceService.findByUser(userService.findByLogin(currentUser.getUsername())));
            model.addAttribute("levels", Level.values());
            return "scripts/editPage";
        }

        editScript.setId(scriptId);
        String result = "fail";
        if (scriptService.save(entityConverter.convertToEntityScript(editScript)) != null) {
            result = "success";
        }
        return "redirect:/scripts"  + "?edit=" + result;
    }

    @RequestMapping(value = "/script/create", method = RequestMethod.POST)
    public String createScript(@ModelAttribute("script") @Valid Script script, BindingResult bindingResult, Model model) {

        if (bindingResult.hasErrors()) {
            model.addAttribute("script", script);
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            currentUser = (User) authentication.getPrincipal();
            List<ru.hse.gureevnd.ui.persistance.entity.Source> sources = sourceService.findByUser(userService.findByLogin(currentUser.getUsername()));
            model.addAttribute("sources", sources);
            model.addAttribute("levels", Level.values());
            return "scripts/createPage";
        }

        String result = "fail";
        if (scriptService.save(entityConverter.convertToEntityScript(script)) != null) {
            result = "success";
        }
        return "redirect:/scripts"  + "?create=" + result;
    }

    @RequestMapping(value = "/script/create", method = RequestMethod.GET)
    public String createScript(Model model) {
        model.addAttribute("script", new Script());
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        currentUser = (User) authentication.getPrincipal();
        List<ru.hse.gureevnd.ui.persistance.entity.Source> sources = sourceService.findByUser(userService.findByLogin(currentUser.getUsername()));
        model.addAttribute("sources", sources);
        model.addAttribute("levels", Level.values());
        return "scripts/createPage";
    }

    @RequestMapping(value = "/script/create/{sourceId}", method = RequestMethod.GET)
    public String createScript(Model model, @PathVariable("sourceId") long source) {
        Script script = new Script();
        script.setSource(source);
        model.addAttribute("script", script);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        currentUser = (User) authentication.getPrincipal();
        List<ru.hse.gureevnd.ui.persistance.entity.Source> sources = sourceService.findByUser(userService.findByLogin(currentUser.getUsername()));
        model.addAttribute("sources", sources);
        model.addAttribute("levels", Level.values());
        return "scripts/createPage";
    }

    @RequestMapping(value = "/script/{sourceId}/display", method = RequestMethod.GET)
    public String displayScriptsBySource(Model model, @PathVariable("sourceId") long source) {
        model.addAttribute("allScriptsForSource", scriptService.findBySource(source));
        model.addAttribute("source", source);
        return "scripts/listPageBySource";
    }
}
