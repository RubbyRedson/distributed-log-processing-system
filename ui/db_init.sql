create table if not exists users ( 
id SERIAL PRIMARY KEY, 
login varchar(100) NOT NULL, 
password varchar(100) NOT NULL 
); 
create table if not exists sources ( 
id SERIAL PRIMARY KEY, 
user_id integer REFERENCES users(id) NOT NULL, 
name varchar(100) NOT NULL, 
ip varchar(100) NOT NULL, 
login varchar(100), 
password varchar(100) 
); 
create table if not exists groups (id SERIAL PRIMARY KEY, 
user_id integer REFERENCES users(id) NOT NULL, 
name varchar(100) 
); 
create table if not exists alerts (id SERIAL PRIMARY KEY, 
source_id integer REFERENCES sources (id) NOT NULL, 
name varchar(100) NOT NULL, 
level varchar(25) NOT NULL, 
email varchar(100) NOT NULL, 
topic varchar(100) NOT NULL, 
template varchar(1023) NOT NULL, 
pattern varchar(255) 
); 
create table if not exists scripts ( 
id SERIAL PRIMARY KEY, 
source_id integer REFERENCES sources (id) NOT NULL, 
name varchar(100) NOT NULL, 
level varchar(25) NOT NULL, 
path varchar(255) NOT NULL, 
pattern varchar(255) 
); 
create table if not exists logs ( 
id SERIAL PRIMARY KEY, 
source_id integer REFERENCES sources (id) NOT NULL, 
level varchar(25) NOT NULL, 
text varchar(255), 
time timestamp NOT NULL 
); 
create table if not exists source_groups ( 
source_id integer REFERENCES sources (id) NOT NULL, 
group_id integer REFERENCES groups (id) NOT NULL 
);