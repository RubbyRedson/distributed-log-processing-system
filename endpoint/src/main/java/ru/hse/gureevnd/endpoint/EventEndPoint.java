package ru.hse.gureevnd.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import ru.hse.gureevnd.dto.events.EventDTO;
import ru.hse.gureevnd.kafka.ProducerService;
import ru.hse.gureevnd.kafka.events.Event;
import ru.hse.gureevnd.util.DTOConverter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.concurrent.ExecutionException;

@WebServlet(name = "/",
        urlPatterns = {"/"})
public class EventEndPoint extends HttpServlet {
    private ObjectMapper objectMapper;
    private ProducerService producerService;

    @Override
    public void init() throws ServletException {
        objectMapper  = new ObjectMapper();
        producerService = new ProducerService("event-topic");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Event event = objectMapper.readValue(req.getReader(), Event.class);
        System.out.println("doPost event: "+event.toString());
        //TODO: exception handlers for incorrect JSON
        EventDTO eventDTO = DTOConverter.toDTO(event);
        System.out.println(eventDTO);
        try {
            producerService.sendEvent(eventDTO);
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void destroy() {
        producerService.stop();
    }
}
