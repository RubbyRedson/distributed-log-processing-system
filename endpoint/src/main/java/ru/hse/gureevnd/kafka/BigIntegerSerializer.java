package ru.hse.gureevnd.kafka;

import org.apache.kafka.common.serialization.Serializer;

import java.math.BigInteger;
import java.util.Map;

public class BigIntegerSerializer implements Serializer<BigInteger> {
    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {}

    @Override
    public byte[] serialize(String topic, BigInteger data) {
        return data.toByteArray();
    }

    @Override
    public void close() {}
}
