package ru.hse.gureevnd.kafka;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import ru.hse.gureevnd.dto.events.EventDTO;

import java.math.BigInteger;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class ProducerService {
    private final String topic;
    private final KafkaProducer<BigInteger, EventDTO> producer;

    public ProducerService(String topic) {
        this.topic = topic;
        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "146.148.14.167:9092");
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, EventDTOSerializer.class.getName());
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, BigIntegerSerializer.class.getName());
        props.put(ProducerConfig.PARTITIONER_CLASS_CONFIG, CustomPartitioner.class.getName());
        producer = new KafkaProducer<>(props);

    }

    public void sendEvent(EventDTO event) throws ExecutionException, InterruptedException {
        Future<RecordMetadata> send =
                producer.send(new ProducerRecord<>(topic, event.sourceId, event));
    }

    public void stop() {
        producer.close();
    }

}
