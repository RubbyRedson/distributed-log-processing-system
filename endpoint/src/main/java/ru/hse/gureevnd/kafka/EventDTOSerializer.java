package ru.hse.gureevnd.kafka;

import org.apache.kafka.common.serialization.Serializer;
import ru.hse.gureevnd.dto.events.EventDTO;


import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Map;

public class EventDTOSerializer implements Serializer<EventDTO> {

    //TODO: add logging

    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
        // nothing to do
    }

    @Override
    public byte[] serialize(String topic, EventDTO event) {
        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            try (ObjectOutputStream out = new ObjectOutputStream(outputStream)) {
                out.writeObject(event);
                return outputStream.toByteArray();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new byte[0];
    }

    @Override
    public void close() {
        // nothing to do
    }
}
