package ru.hse.gureevnd.kafka;


import org.apache.kafka.clients.producer.Partitioner;
import org.apache.kafka.common.Cluster;

import java.math.BigInteger;
import java.util.Map;

public class CustomPartitioner implements Partitioner {

    @Override
    public int partition(String topic, Object key, byte[] keyBytes, Object value, byte[] valueBytes, Cluster cluster) {
        Integer partitionNumber = cluster.partitionCountForTopic(topic);
        System.out.println("partitionNumber="+partitionNumber);
        int eventKey = ((BigInteger) key).intValue();
        //TODO: normal hash-function
        int result = Math.abs(eventKey * 17) % partitionNumber;
        System.out.println("Partition bucket="+result);
        return result;
    }

    @Override
    public void close() {
    }

    @Override
    public void configure(Map<String, ?> configs) {
    }
}
