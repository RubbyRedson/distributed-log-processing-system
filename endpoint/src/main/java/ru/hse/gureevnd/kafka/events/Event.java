package ru.hse.gureevnd.kafka.events;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import ru.hse.gureevnd.util.CustomJsonDateDeserializer;
import ru.hse.gureevnd.util.CustomJsonDateSerializer;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
/**
 * Created by Nikita on 22.10.2015.
 */
@JsonTypeName("logging-event")
public class Event implements Serializable {

	private static final long serialVersionUID = 782154693;

	@JsonProperty("time-sent")
	@JsonDeserialize(using = CustomJsonDateDeserializer.class)
	@JsonSerialize(using = CustomJsonDateSerializer.class)
	private Date timeSent;

	@JsonProperty("source-id")
	private BigInteger sourceId;

	public BigInteger getSourceId() {
		return sourceId;
	}

	public Date getTimeSent() {
		return timeSent;
	}


	@JsonProperty("level")
	private LoggingLevel level;

	@JsonProperty("text")
	private String message;

	public String getMessage() {
		return message;
	}

	public LoggingLevel getLevel() {
		return level;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Event event = (Event) o;

		if (timeSent != null ? !timeSent.equals(event.timeSent) : event.timeSent != null) return false;
		if (sourceId != null ? !sourceId.equals(event.sourceId) : event.sourceId != null) return false;
		if (level != event.level) return false;
		return message != null ? message.equals(event.message) : event.message == null;

	}

	@Override
	public int hashCode() {
		int result = timeSent != null ? timeSent.hashCode() : 0;
		result = 31 * result + (sourceId != null ? sourceId.hashCode() : 0);
		result = 31 * result + (level != null ? level.hashCode() : 0);
		result = 31 * result + (message != null ? message.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "Event{" +
				"timeSent=" + timeSent +
				", sourceId=" + sourceId +
				", level=" + level +
				", message='" + message + '\'' +
				'}';
	}

	public enum LoggingLevel {
		DEBUG("debug"), INFO("info"), WARN("warn"), ERROR("error"), FATAL("fatal"), UNKNOWN(null);

		LoggingLevel(String level) {
			this.level = level;
		}

		private String level;

		@JsonValue
		public String getLevel() {
			return level;
		}

		@JsonCreator
		public static LoggingLevel fromString(String level) {
			if (level != null) {
				for (LoggingLevel enumLevel : LoggingLevel.values()) {
					if (level.equals(enumLevel.getLevel()))
						return enumLevel;
				}
			}
			return UNKNOWN;
		}
	}
}
