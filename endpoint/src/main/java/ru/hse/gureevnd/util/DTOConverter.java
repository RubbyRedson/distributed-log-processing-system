package ru.hse.gureevnd.util;

import ru.hse.gureevnd.dto.events.EventDTO;
import ru.hse.gureevnd.dto.events.LoggingEventDTO;
import ru.hse.gureevnd.kafka.events.Event;


public class DTOConverter {
    public static EventDTO toDTO(Event event) {
        return new LoggingEventDTO(event.getSourceId(),
                event.getTimeSent(),
                event.getLevel().getLevel(),
                event.getMessage());
    }
}
