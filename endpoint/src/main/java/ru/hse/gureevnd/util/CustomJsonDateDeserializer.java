package ru.hse.gureevnd.util;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CustomJsonDateDeserializer extends JsonDeserializer<Date> {
    public final static String STRING_DATE_FORMAT = "dd.MM.yyyy";
    public final static String STRING_DATE_FORMAT_WITH_TIME = "dd.MM.yyyy HH:mm:ss.SSS";
    public final static ThreadLocal<SimpleDateFormat> DATE_FORMATTER = new ThreadLocal<SimpleDateFormat>() {
        @Override
        protected SimpleDateFormat initialValue() {
            return new SimpleDateFormat(STRING_DATE_FORMAT);
        }
    };
    public final static ThreadLocal<SimpleDateFormat> DATE_FORMATTER_WITH_TIME = new ThreadLocal<SimpleDateFormat>() {
        @Override
        protected SimpleDateFormat initialValue() {
            return new SimpleDateFormat(STRING_DATE_FORMAT_WITH_TIME);
        }
    };

    @Override
    public Date deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        String date = jsonParser.getText();
        try {
            return parseDate(date);
        } catch (ParseException e) {
            throw new RuntimeException("Incorrect date format");
        }
    }

    public static Date parseDate(String date) throws ParseException {
        try {
            return DATE_FORMATTER_WITH_TIME.get().parse(date);
        } catch (ParseException e) {
            return DATE_FORMATTER.get().parse(date);
        }
    }




}
