package ru.hse.gureevnd.util;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CustomJsonDateSerializer extends JsonSerializer<Date>{
    @Override
    public void serialize(Date date, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeString(formatDate(date));
    }

    public static String formatDate(Date date) {
        if (CustomJsonDateDeserializer.DATE_FORMATTER_WITH_TIME.get() == null) {
            CustomJsonDateDeserializer.DATE_FORMATTER_WITH_TIME.set(
                    new SimpleDateFormat(CustomJsonDateDeserializer.STRING_DATE_FORMAT_WITH_TIME));
        }
        return CustomJsonDateDeserializer.DATE_FORMATTER_WITH_TIME.get().format(date);
    }
}
