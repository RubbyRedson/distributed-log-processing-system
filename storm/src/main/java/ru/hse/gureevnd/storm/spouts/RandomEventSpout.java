package ru.hse.gureevnd.storm.spouts;

import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import backtype.storm.utils.Utils;

import java.security.SecureRandom;
import java.util.Calendar;
import java.util.Map;
import java.util.Random;

/**
 * Created by Nikita on 26.12.2015.
 */
public class RandomEventSpout extends BaseRichSpout {
	private SpoutOutputCollector collector;
	private Random rand = new SecureRandom();
	int counter = 5;
	boolean f = true;

	@Override
	public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
		outputFieldsDeclarer.declare(new Fields("source_id", "level", "text", "time"));
	}

	@Override
	public void open(Map map, TopologyContext topologyContext, SpoutOutputCollector spoutOutputCollector) {
		this.collector = spoutOutputCollector;
	}

	@Override
	public void nextTuple() {
		counter++;
		try {
			Thread.sleep(10 * 15);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		collector.emit(new Values(18,
				 "ERROR", "test message", new java.sql.Timestamp(Calendar.getInstance().getTime().getTime())));
	}
}
