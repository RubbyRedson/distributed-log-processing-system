package ru.hse.gureevnd.storm.bolts;

import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import org.slf4j.LoggerFactory;
import ru.hse.gureevnd.util.ScriptExecutor;

import java.sql.Timestamp;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Nikita on 10.04.2016.
 */
public class ScriptsBolt  extends BaseBasicBolt {
	private static final org.slf4j.Logger CUSTOM_LOGGER = LoggerFactory.getLogger("customLog");

	@Override
	public void execute(Tuple tuple, BasicOutputCollector collector) {
		CUSTOM_LOGGER.error("Script received " + tuple.toString());
		collector.emit(new Values(0));

		int sourceId = tuple.getIntegerByField("source_id");
		String level = tuple.getStringByField("level");
		String text = tuple.getStringByField("text");
		Timestamp time = (Timestamp) tuple.getValueByField("time");
		String ip = tuple.getStringByField("ip");
		String login = tuple.getStringByField("login");
		String password = tuple.getStringByField("password");
		String path = tuple.getStringByField("path");
		String pattern = tuple.getStringByField("pattern");

		if (pattern != null && !pattern.isEmpty()) {
			Pattern regex = Pattern.compile(pattern);
			Matcher m = regex.matcher(text);
			if (m.matches()) {
				ScriptExecutor.executeFile(login, password,
						ip, path);
			} else {
				CUSTOM_LOGGER.error("Not matched");
			}
		} else {
			ScriptExecutor.executeFile(login, password,
									ip, path);
		}
		collector.emit(new Values(1));
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer ofd) {
		ofd.declare(new Fields("result"));
	}


}
