package ru.hse.gureevnd.storm.bolts.jdbc;

import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.ITuple;
import backtype.storm.tuple.Values;
import org.apache.storm.jdbc.common.Column;
import org.apache.storm.jdbc.mapper.JdbcLookupMapper;
import ru.hse.gureevnd.dto.events.LoggingEventDTO;

import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nikita on 10.04.2016.
 */
public class ScriptLookupMapper implements JdbcLookupMapper {
	@Override
	public List<Values> toTuple(ITuple iTuple, List<Column> list) {
		Values values = new Values();
		Object object = iTuple.getValueByField("object");
		if (object instanceof LoggingEventDTO) {
			LoggingEventDTO eventDTO = (LoggingEventDTO) object;
			List<Column> result = new ArrayList<>();
			result.add(new Column<>("source_id", eventDTO.sourceId.intValue(), Types.INTEGER));
			result.add(new Column<>("level", eventDTO.level.toUpperCase(), Types.VARCHAR));
			values.add(eventDTO.sourceId.intValue());
			values.add(eventDTO.level);
			values.add(eventDTO.message);
			values.add(new java.sql.Timestamp(eventDTO.timeSent.getTime()));
		}

		for (Column col : list) {
			if ("ip".equals(col.getColumnName())) {
				values.add(col.getVal());
			} else if ("login".equals(col.getColumnName())) {
				values.add(col.getVal());
			} else if ("password".equals(col.getColumnName())) {
				values.add(col.getVal());
			} else if ("path".equals(col.getColumnName())) {
				values.add(col.getVal());
			} else if ("pattern".equals(col.getColumnName())) {
				values.add(col.getVal());
			}
		}

		List<Values> result = new ArrayList<>();
		result.add(values);
		return result;
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
		outputFieldsDeclarer.declare(new Fields("source_id", "level", "text", "time",
				"ip", "login", "password", "path", "pattern"));
	}

	@Override
	public List<Column> getColumns(ITuple iTuple) {
		Object object = iTuple.getValueByField("object");
		if (object instanceof LoggingEventDTO) {
			LoggingEventDTO eventDTO = (LoggingEventDTO) object;
			List<Column> result = new ArrayList<>();
			result.add(new Column<>("source_id", eventDTO.sourceId.intValue(), Types.INTEGER));
			result.add(new Column<>("level", eventDTO.level.toUpperCase(), Types.VARCHAR));
			return result;
		}
		else return null;
	}
}
