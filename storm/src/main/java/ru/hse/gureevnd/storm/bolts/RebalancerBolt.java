package ru.hse.gureevnd.storm.bolts;

import backtype.storm.generated.ExecutorSummary;
import backtype.storm.generated.TopologyInfo;
import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Tuple;
import backtype.storm.utils.NimbusClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Nikita on 16.04.2016.
 */
public class RebalancerBolt extends BaseBasicBolt {
	private static final Logger LOG = LoggerFactory.getLogger(RebalancerBolt.class);

	int incoming;
	int processed;

	String topologyName;
	String host;
	String executorName;

	int windowSize = 15;
	double alpha = 2.0 / (windowSize - 1);
	double beta = 1.0 - alpha;

	double incrementThreshold = 0.1;

	double EMA_incoming;
	double EMA_processed;

	long deltaTime = 5000; //milliseconds

	long lastTime;


	public RebalancerBolt(String host, String topologyName, String executorName) {
		this.topologyName = topologyName;
		this.executorName = executorName;
		this.host = host;
	}

	@Override
	public void execute(Tuple tuple, BasicOutputCollector collector) {

		if (0 == tuple.getInteger(0)) {
			incoming++;
		} else if (1 == tuple.getInteger(0)){
			processed++;
		}

		if (System.currentTimeMillis() - lastTime > deltaTime) {
			if (EMA_incoming == 0)
				EMA_incoming = incoming;
			else
				EMA_incoming = alpha * incoming + beta * EMA_incoming;

			if (EMA_processed == 0)
				EMA_processed = processed;
			else
				EMA_processed = alpha * processed + beta * EMA_processed;
			LOG.error("EMA incoming " + EMA_incoming);
			LOG.error("EMA processed " + EMA_processed);
			incoming = 0;
			processed = 0;
			lastTime = System.currentTimeMillis();

			if (EMA_processed != 0 && EMA_incoming / EMA_processed > incrementThreshold) {
				int additional = (int) ((EMA_incoming / EMA_processed) / incrementThreshold);
				int numberOfExecutors = 1;
				Map<String, Integer> executors = new HashMap<>();
				executors.put(executorName, additional + numberOfExecutors);

				LOG.error("Rebalance");
				Runtime rt = Runtime.getRuntime();
				try {
					rt.exec(new String[]{"cmd.exe","storm rebalance -e " + executorName + "=" +
							(additional + numberOfExecutors)});

				} catch (IOException e) {
					e.printStackTrace();
				}

			}
		}
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer ofd) {
	}

	private int getExecutorCount(List<ExecutorSummary> executorSummaries, String name) {
		int result = 0;
		for (ExecutorSummary executorSummary : executorSummaries) {
			if (name.equals(executorSummary.get_component_id()))
				result++;
		}
		return result;
	}
}
