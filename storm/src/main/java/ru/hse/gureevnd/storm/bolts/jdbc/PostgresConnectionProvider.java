package ru.hse.gureevnd.storm.bolts.jdbc;

import org.apache.storm.jdbc.common.ConnectionProvider;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by Nikita on 10.04.2016.
 */
public class PostgresConnectionProvider implements ConnectionProvider {

	@Override
	public void prepare() {
		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

	}

	@Override
	public Connection getConnection() {
		Connection connection = null;
		try {
			connection = DriverManager.getConnection(
					"jdbc:postgresql://localhost/postgres","postgres", "root");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return connection;
	}

	@Override
	public void cleanup() {

	}
}
