package ru.hse.gureevnd.storm.bolts.jdbc;

import org.apache.storm.jdbc.common.ConnectionProvider;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by Nick on 5/19/2016.
 */
public class SqlServerConnectionProvider implements ConnectionProvider {

    @Override
    public void prepare() {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    @Override
    public Connection getConnection() {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(
                    "jdbc:sqlserver://dlps-databse-server.database.windows.net:1433;database=dlps-database;encrypt=true;" +
                            "trustServerCertificate=false;hostNameInCertificate=*.database.windows.net;loginTimeout=30;",
                    "Nikita@dlps-databse-server", "132284130Kdg1z");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

    @Override
    public void cleanup() {

    }
}
