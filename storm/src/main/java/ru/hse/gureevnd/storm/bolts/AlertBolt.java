package ru.hse.gureevnd.storm.bolts;

import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import com.sendgrid.SendGrid;
import com.sendgrid.SendGridException;
import org.slf4j.LoggerFactory;
import ru.hse.gureevnd.util.GoogleMail;
import ru.hse.gureevnd.util.MacroReplacer;
import ru.hse.gureevnd.util.SendGridMail;

import javax.mail.MessagingException;
import java.sql.Timestamp;

/**
 * Created by Nikita on 16.01.2016.
 */
public class AlertBolt extends BaseBasicBolt {
	private static final org.slf4j.Logger CUSTOM_LOGGER = LoggerFactory.getLogger("customLog");

	@Override
	public void execute(Tuple tuple, BasicOutputCollector collector) {
		CUSTOM_LOGGER.error("Alert received " + tuple.toString());
		collector.emit(new Values(0));

		int sourceId = tuple.getIntegerByField("source_id");
		String level = tuple.getStringByField("level");
		String text = tuple.getStringByField("text");
		Timestamp time = (Timestamp) tuple.getValueByField("time");
		String email = tuple.getStringByField("email");
		String topic = tuple.getStringByField("topic");
		String template = tuple.getStringByField("template");


		String subject = MacroReplacer.replace(topic, Integer.toString(sourceId), level, text, time.toString());
		String body = MacroReplacer.replace(template, Integer.toString(sourceId), level, text, time.toString());

			try {
				SendGridMail.send(email, subject, body);
			} catch (MessagingException | SendGridException e) {
				CUSTOM_LOGGER.error("Exception while sending mail", e);
			}
		collector.emit(new Values(1));
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer ofd) {
		ofd.declare(new Fields("result"));
	}


}