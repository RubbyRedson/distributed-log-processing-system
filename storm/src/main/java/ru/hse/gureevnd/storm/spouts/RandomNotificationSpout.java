package ru.hse.gureevnd.storm.spouts;

import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;

import java.util.Map;

/**
 * Created by Nikita on 16.01.2016.
 */
public class RandomNotificationSpout extends BaseRichSpout {
	private SpoutOutputCollector collector;

	@Override
	public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
		outputFieldsDeclarer.declare(new Fields("notification", "ts"));
	}

	@Override
	public void open(Map map, TopologyContext topologyContext, SpoutOutputCollector spoutOutputCollector) {
		this.collector = spoutOutputCollector;
	}

	@Override
	public void nextTuple() {
//		Notification notification = new Notification("source", "content", new java.util.Date(), LoggingEvent.LoggingLevel.ERROR);
//		Utils.sleep(100);
//		collector.emit(new Values(notification, System.currentTimeMillis() - (24 * 60 * 60 * 1000)));

	}
}