package ru.hse.gureevnd.storm.bolts.jdbc;

import backtype.storm.tuple.ITuple;
import org.apache.storm.jdbc.common.Column;
import org.apache.storm.jdbc.mapper.JdbcMapper;
import ru.hse.gureevnd.dto.events.LoggingEventDTO;

import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nikita on 10.04.2016.
 */
public class LogInsertMapper implements JdbcMapper {

	@Override
	public List<Column> getColumns(ITuple iTuple) {
		Object object = iTuple.getValueByField("object");
		if (object instanceof LoggingEventDTO) {
			LoggingEventDTO eventDTO = (LoggingEventDTO) object;
			List<Column> result = new ArrayList<>();
			result.add(new Column<>("source_id", eventDTO.sourceId.intValue(), Types.INTEGER));
			result.add(new Column<>("level", eventDTO.level.toUpperCase(), Types.VARCHAR));
			result.add(new Column<>("text", eventDTO.message, Types.VARCHAR));
			result.add(new Column<>("time", new java.sql.Timestamp(eventDTO.timeSent.getTime()), Types.TIMESTAMP));
			return result;
		}
		else return null;
	}

}
