package ru.hse.gureevnd.storm.bolts;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import org.apache.commons.lang3.StringUtils;
import org.apache.storm.jdbc.bolt.AbstractJdbcBolt;
import org.apache.storm.jdbc.common.ConnectionProvider;
import org.apache.storm.jdbc.mapper.JdbcMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Nikita on 16.04.2016.
 */
public class LogInsertBolt extends AbstractJdbcBolt {
	private static final Logger LOG = LoggerFactory.getLogger(org.apache.storm.jdbc.bolt.JdbcInsertBolt.class);
	private String tableName;
	private String insertQuery;
	private JdbcMapper jdbcMapper;

	public LogInsertBolt(ConnectionProvider connectionProvider, JdbcMapper jdbcMapper) {
		super(connectionProvider);
		this.jdbcMapper = jdbcMapper;
	}

	public LogInsertBolt withTableName(String tableName) {
		this.tableName = tableName;
		return this;
	}

	public LogInsertBolt withInsertQuery(String insertQuery) {
		this.insertQuery = insertQuery;
		return this;
	}

	public LogInsertBolt withQueryTimeoutSecs(int queryTimeoutSecs) {
		this.queryTimeoutSecs = queryTimeoutSecs;
		return this;
	}

	public void prepare(Map map, TopologyContext topologyContext, OutputCollector collector) {
		super.prepare(map, topologyContext, collector);
		if(StringUtils.isBlank(this.tableName) && StringUtils.isBlank(this.insertQuery)) {
			throw new IllegalArgumentException("You must supply either a tableName or an insert Query.");
		}
	}

	public void execute(Tuple tuple) {
		try {
			collector.emit(new Values(0));
			List e = this.jdbcMapper.getColumns(tuple);
			ArrayList columnLists = new ArrayList();
			columnLists.add(e);
			if(!StringUtils.isBlank(this.tableName)) {
				this.jdbcClient.insert(this.tableName, columnLists);
			} else {
				this.jdbcClient.executeInsertQuery(this.insertQuery, columnLists);
			}

			this.collector.ack(tuple);
			collector.emit(new Values(1));
		} catch (Exception var4) {
			this.collector.reportError(var4);
			this.collector.fail(tuple);
		}

	}

	public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
		outputFieldsDeclarer.declare(new Fields("result"));
	}
}