package ru.hse.gureevnd.storm.schemas;

import backtype.storm.spout.Scheme;
import backtype.storm.tuple.Fields;

import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;

public class JavaBinaryScheme implements Scheme {
    private static final org.slf4j.Logger CUSTOM_LOGGER = LoggerFactory.getLogger("customLog");
    @Override
    public List<Object> deserialize(byte[] bytes) {
        try (ByteArrayInputStream byteInputStream = new ByteArrayInputStream(bytes);
             ObjectInputStream objectInputStream = new ObjectInputStream(byteInputStream)) {
            List<Object> toReturn = new ArrayList<>();
            toReturn.add(objectInputStream.readObject());
            return toReturn;
        } catch (IOException | ClassNotFoundException e) {
            CUSTOM_LOGGER.error("Error while deserializing from bytes", e);
        }
        return null;
    }

    @Override
    public Fields getOutputFields() {
        return new Fields("object");
    }
}
