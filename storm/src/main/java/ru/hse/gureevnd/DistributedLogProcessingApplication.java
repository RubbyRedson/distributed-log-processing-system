package ru.hse.gureevnd;

import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.StormSubmitter;
import backtype.storm.generated.AlreadyAliveException;
import backtype.storm.generated.AuthorizationException;
import backtype.storm.generated.InvalidTopologyException;
import backtype.storm.generated.StormTopology;
import backtype.storm.spout.SchemeAsMultiScheme;
import backtype.storm.topology.TopologyBuilder;
import org.apache.storm.jdbc.bolt.JdbcLookupBolt;
import org.apache.storm.jdbc.common.ConnectionProvider;
import org.apache.thrift7.transport.TTransportException;
import ru.hse.gureevnd.storm.bolts.AlertBolt;
import ru.hse.gureevnd.storm.bolts.LogInsertBolt;
import ru.hse.gureevnd.storm.bolts.RebalancerBolt;
import ru.hse.gureevnd.storm.bolts.ScriptsBolt;
import ru.hse.gureevnd.storm.bolts.jdbc.*;
import ru.hse.gureevnd.storm.schemas.JavaBinaryScheme;
import ru.hse.gureevnd.storm.spouts.RandomEventSpout;
import storm.kafka.BrokerHosts;
import storm.kafka.KafkaSpout;
import storm.kafka.SpoutConfig;
import storm.kafka.ZkHosts;

import javax.mail.MessagingException;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * Created by Nikita on 17.03.2016.
 */
public class DistributedLogProcessingApplication {

	static LocalCluster cluster = new LocalCluster();
	public static void main(String[] args) throws MessagingException, InterruptedException, TTransportException, InvalidTopologyException, AuthorizationException, AlreadyAliveException {

		ConnectionProvider connectionProvider = new SqlServerConnectionProvider();

		LogInsertBolt logPersistanceBolt = new LogInsertBolt(connectionProvider, new LogInsertMapper())
				.withInsertQuery("insert into logs(source_id, level, text, time) VALUES(?, ?, ?, ?)")
				.withQueryTimeoutSecs(30);

		String selectAlertQuery = "select email, topic, template from alerts where source_id = ? and upper(level) = upper(?)";

		JdbcLookupBolt alertLookupBolt = new JdbcLookupBolt(connectionProvider, selectAlertQuery,
				new AlertLookupMapper());

		String selectScriptsQuery = "select so.ip, so.login, so.password, s.path, s.pattern from scripts s, sources so " +
				"where s.source_id = ? and upper(s.level) = upper(?) and so.id = s.source_id and (so.login is not null or so.login <> '')" +
				"and (so.password is not null)";

		JdbcLookupBolt scriptsLookupBolt = new JdbcLookupBolt(connectionProvider, selectScriptsQuery,
				new ScriptLookupMapper());

		String host = "nimbus";
		String topologyName = "dlps";
		int port = 6627;

		String first = "146.148.14.167:2181";
//		String second = "146.185.191.227:2181";
		String zkConnString = first;
		String topicName = "event-topic";

		BrokerHosts hosts = new ZkHosts(zkConnString);
		SpoutConfig spoutConfig = new SpoutConfig(hosts, topicName, "/" + topicName, UUID.randomUUID().toString());
		spoutConfig.scheme = new SchemeAsMultiScheme(new JavaBinaryScheme());


		KafkaSpout kafkaSpout = new KafkaSpout(spoutConfig);

		TopologyBuilder builder = new TopologyBuilder();
		builder.setSpout("kafkaSpout", kafkaSpout, 1);
		builder.setBolt("AlertLookupBolt", alertLookupBolt, 1).allGrouping("kafkaSpout");
		builder.setBolt("ScriptsLookupBolt", scriptsLookupBolt, 1).allGrouping("kafkaSpout");
		builder.setBolt("LogPersistanceBolt", logPersistanceBolt, 1).allGrouping("kafkaSpout");
		builder.setBolt("AlertBolt", new AlertBolt(), 1).shuffleGrouping("AlertLookupBolt");
		builder.setBolt("ScriptsBolt", new ScriptsBolt(), 1).shuffleGrouping("ScriptsLookupBolt");

		//balancing
		builder.setBolt("LogPersistanceBalancer", new RebalancerBolt(host, topologyName, "LogPersistanceBolt"),
				1).shuffleGrouping("LogPersistanceBolt");
		builder.setBolt("AlertBalancer", new RebalancerBolt(host, topologyName, "AlertBolt"),
				1).shuffleGrouping("AlertBolt");
		builder.setBolt("ScriptsBalancer", new RebalancerBolt(host, topologyName, "ScriptsBolt"),
				1).shuffleGrouping("ScriptsBolt");

		StormSubmitter.submitTopology("dlps", createEventConfig(), builder.createTopology());
//		cluster.submitTopology(topologyName, createEventConfig(), builder.createTopology());
////		Thread.sleep(1000 * 300);
//		TimeUnit.MINUTES.sleep(5);
//		cluster.shutdown();
	}

	public static Config createEventConfig() {
		Config config = new Config();
		config.setDebug(true);
		config.setNumWorkers(4);
		return config;
	}

//	public static StormTopology createKafkaEventTopology() {
//		String first = "46.101.131.133:2181";
//		String second = "146.185.191.227:2181";
//		String zkConnString = first + "," + second;
//		String topicName = eventTopic;
//
//		BrokerHosts hosts = new ZkHosts(zkConnString);
//		SpoutConfig spoutConfig = new SpoutConfig(hosts, topicName, "/" + topicName, UUID.randomUUID().toString());
//		spoutConfig.scheme = new SchemeAsMultiScheme(new JavaBinaryScheme());
//
//
//		KafkaSpout kafkaSpout = new KafkaSpout(spoutConfig);
//
//		TopologyBuilder builder = new TopologyBuilder();
//		builder.setSpout("kafkaEvent", kafkaSpout, 1);  // Spout parallelism
//		builder.setBolt("LoggerBolt", new LoggerBolt(), 1).shuffleGrouping("kafkaEvent");
//		builder.setBolt("AlertBolt", new AlertBolt(), 1).shuffleGrouping("LoggerBolt", "alerts");
//		builder.setBolt("DatabaseWriterBolt", new DatabaseWriterBolt(), 1).shuffleGrouping("LoggerBolt", "events");
//		return builder.createTopology();
//	}
}
