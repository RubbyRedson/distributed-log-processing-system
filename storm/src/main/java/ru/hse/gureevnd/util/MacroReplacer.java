package ru.hse.gureevnd.util;

import org.apache.commons.lang3.StringUtils;

public class MacroReplacer {
		private static final String SOURCE_ID = "${source_id}";
		private static final String TIME = "${time}";
		private static final String LEVEL = "${level}";
		private static final String TEXT = "${text}";

		private static final String[] LOG_SEARCH_LIST =  new String[]
				{SOURCE_ID, LEVEL, TEXT, TIME};



		public static String replace(String input, String source, String level, String text,
										  String time) {
			return StringUtils.replaceEach(input, LOG_SEARCH_LIST, new String[] {source, level, text,
					time});
		}

}
