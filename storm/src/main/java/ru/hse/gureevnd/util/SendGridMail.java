package ru.hse.gureevnd.util;
import com.sendgrid.SendGrid;
import com.sendgrid.SendGridException;

import javax.mail.MessagingException;


/**
 * Created by Nick on 5/30/2016.
 */
public class SendGridMail {
    private SendGridMail() {
    }

    public static void send(String recipientEmail, String title, String message) throws MessagingException, SendGridException {
        send(recipientEmail, "", title, message);
    }

    public static void send(String recipientEmail, String ccEmail, String title, String message) throws MessagingException, SendGridException {
        SendGrid sendgrid = new SendGrid("rubbyredson", "2k0MvjJTl5");

        SendGrid.Email email = new SendGrid.Email();

        email.addTo(recipientEmail);
        email.setFrom("dlps.notifications@gmail.com");
        email.setFromName("DLPS Alerts");
        email.setSubject(title);
        email.setHtml(message);
        SendGrid.Response response = sendgrid.send(email);
        System.out.println(response.getMessage());
    }

    public static void main(String[] args) throws MessagingException, SendGridException {
        send("gurnik2009@gmail.com", "Test Subject", "Test Body");

    }
}
