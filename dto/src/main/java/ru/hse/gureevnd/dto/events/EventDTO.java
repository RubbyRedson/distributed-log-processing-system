package ru.hse.gureevnd.dto.events;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;


public abstract class EventDTO implements Serializable {
    public BigInteger sourceId;
    public Date timeSent;


    public EventDTO(BigInteger sourceId, Date timeSent) {
        this.sourceId = sourceId;
        this.timeSent = timeSent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EventDTO)) return false;

        EventDTO eventDTO = (EventDTO) o;

        if (sourceId != null ? !sourceId.equals(eventDTO.sourceId) : eventDTO.sourceId != null)
            return false;
        return timeSent != null ? timeSent.equals(eventDTO.timeSent) : eventDTO.timeSent == null;

    }

    @Override
    public int hashCode() {
        int result = sourceId != null ? sourceId.hashCode() : 0;
        result = 31 * result + (sourceId != null ? sourceId.hashCode() : 0);
        result = 31 * result + (timeSent != null ? timeSent.hashCode() : 0);
        return result;
    }
}
