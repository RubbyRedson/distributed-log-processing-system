package ru.hse.gureevnd.dto.events;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.Objects;


public class LoggingEventDTO extends EventDTO implements Serializable {

    private static final long serialVersionUID = 782154693;
    public String level;
    public String message;

    public LoggingEventDTO(BigInteger sourceId, Date timeSent, String level, String message) {
        super(sourceId, timeSent);
        this.level = level;
        this.message = message;
    }

    @Override
    public String toString() {
        return "LoggingEventDTO{" +
                "timeSent=" + timeSent.toString() +
                ", sourceId=" + sourceId +
                ", level=" + level +
                ", message='" + message + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LoggingEventDTO)) return false;
        if (!super.equals(o)) return false;

        LoggingEventDTO that = (LoggingEventDTO) o;
        if (!Objects.equals(level, that.level)) return false;
        if (message != null ? !message.equals(that.message) : that.message != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (level != null ? level.hashCode() : 0);
        result = 31 * result + (message != null ? message.hashCode() : 0);
        return result;
    }


}
